openssl pkcs12 -nocerts -out APNSKey.pem -in key.p12

openssl x509 -in aps.cer -inform der -out prodAPNSCert.pem

cat prodAPNSCert.pem APNSKey.pem > prodAPNSCertAndKey.pem

rm prodAPNSCert.pem

rm APNSKey.pem
