//
//  WebViewController.m
//  pickalbatros
//
//  Created by User on 13.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "WebViewController.h"
#import "MBProgressHUD.h"

@interface WebViewController ()

@end

@implementation WebViewController

+(WebViewController*)getWebViewController{
    WebViewController *wvc = [WebViewController loadNowFromStoryboard:@"WebView"];
    
    return wvc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.webView.delegate = self;
    
    if(!self.urlGiven){
        self.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",SYSTEM_DOMAIN]]; //if it's not given use
    }
    
    //prepare request for the webView
    NSURLRequest *request = [NSURLRequest requestWithURL:self.url cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:30.0];
    
    if(self.useHTML){
        // workaround but the only possible solution:
        // wrapping the content in a span, this sets a "default" font, if nothing is given in the html
        // we dont have access to modify the default font of the webkit
        _htmlString = [NSString stringWithFormat:@"<span style=\"font-family: %@;\">%@</span>",
         @"Helvetica",
         _htmlString];
        [self.webView loadHTMLString:self.htmlString baseURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",SYSTEM_DOMAIN]]];
    } else {
        //load the request
        [self.webView loadRequest:request];
        NSLog(@"%@",self.url);
        
        
        self.isLoading = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSLog(@" web title 1 => %@ ", self.titleString);
    NSLog(@" web => %@ ", self.navigationItem.titleView);
    
    if(self.navigationItem.titleView == nil) {
        self.navigationItem.titleView = [NavigationTitle createNavTitle:self.titleString SubTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"]];
    }
}


- (void) webViewDidStartLoad:(UIWebView *)webView{
    
    self.isLoading = YES;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedString(@"Loading",@"");
}

- (void) webViewDidFinishLoad:(UIWebView *)webView{
    self.isLoading = NO;
    
    self.currentURL = self.webView.request.URL.absoluteString;
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    
    [self.webView setBackgroundColor:[UIColor whiteColor]];
    [self.webView setOpaque:YES];
    
}

- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    if(error.code==NSURLErrorCancelled){
        return;
    }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        UIAlertView *webError = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error",@"") message:[error localizedDescription] delegate:nil cancelButtonTitle:NSLocalizedString(@"Cancel",@"") otherButtonTitles: nil];
        
        [webError show];
    
    NSLog(@"%@", error);
}

-(void)hideModalWebView{
    if(self.navigationController!=nil){
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
@implementation NSURLRequest(DataController)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return NO;
}
@end