//
//  CampaignSelectionViewController.h
//  if_framework_terminal
//
//  Created by User on 5/20/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "iFeedbackViewController.h"

@interface CampaignSelectionViewController : iFeedbackViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UIView *headerView;

@property (nonatomic) NSArray *campaigns;

@property (weak, nonatomic) IBOutlet UIButton *languageSelectorButton;

@property (weak, nonatomic) IBOutlet UIImageView *languageSelectorImageFlag;

@property (weak, nonatomic) IBOutlet UIView *footerBackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *footerImage;

@end
