//
//  ifbck.h
//  nativeifbckapp
//
//  Created by User on 6/2/16.
//  Copyright © 2016 BHM Media Solutions. All rights reserved.
//

#import "ifbckConfig.pch"

#import <Foundation/Foundation.h>
#import "iFeedbackModel.h"
#import "ifbckGlobals.h"

#define kApplicationIdleTimeoutNotification @"ApplicationIdleTimeout"

@protocol ifbckDelegate <NSObject>

@optional -(void)ifbckDidLoadViewController:(UIViewController*)viewController;

@optional -(void)ifbckDidLoadViewController:(UIViewController*)viewController atIndex:(NSInteger)index;

@end

@interface ifbck : NSObject <iFeedbackModelDelegate>

@property (nonatomic) id<ifbckDelegate> delegate;

@property (nonatomic) float navigationBarHeight;

- (void)loadViewController;

- (UIViewController*)getOnboardingViewController;

- (void)loadViewControllerForEntryPoint:(NSString*)entryPoint;

- (void)loadViewControllerForURL:(NSString*)url;

- (void)loadViewControllerForURL:(NSString*)url atIndex:(NSInteger)index;

- (void)loadIFeedbackViewController;

//+(ifbck*)new;

@end
