//
//  SecretSettingsViewController.m
//  if_framework_terminal
//
//  Created by User on 4/12/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "ifbckSecretSettingsViewController.h"

@interface ifbckSecretSettingsViewController ()

@end

@implementation ifbckSecretSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)crashButtonPressed:(id)sender {
    NSException *e = [NSException
                      exceptionWithName:@"AppManuallyCrashed"
                      reason:@"You crashed the app"
                      userInfo:nil];
    @throw e;
}

@end
