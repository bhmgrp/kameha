//
//  iFeedbackTableViewCell.h
//  if_framework
//
//  Created by User on 6/14/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface iFeedbackTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *constraintsToResizeBy1of2;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *constraintsToResizeBy3of4;

-(void)resizeFontFor:(UIView*)item;

@end
