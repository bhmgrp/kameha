//
//  DynamicFormTableViewCell.m
//  if_framework
//
//  Created by User on 31.08.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "DynamicFormTableViewCell.h"

@interface DynamicFormTableViewCell()


@end

@implementation DynamicFormTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    self.textView.layer.cornerRadius = 5.0f;
    _slider.continuous = NO;
    
    if(IDIOM!=IPAD){
        //[_starRatingSelectedValueLabel removeFromSuperview];
        [self resizeFontFor:_starRatingSelectedValueLabel];
        [self resizeFontFor:_starRatingCommentDescription];
        [self resizeFontFor:_label];
        [self resizeFontFor:_dropdownSelectedValueLabel];
        [self resizeFontFor:_textView];
        [self resizeFontFor:_textInput];
    }
    
    
    _starRatingSelectedValueLabel.text = [self getStarRatingValueDescription:0];
}

-(void)starRatingValueChanged:(HCSStarRatingView*)starRatingView{
    
    _starRatingSelectedValueLabel.text = [self getStarRatingValueDescription:(int)starRatingView.value];
    
    //if([self.delegate respondsToSelector:@selector(starRatingValueChanged:)]){
        //[self.delegate starRatingValueChanged:(HCSStarRatingView*)starRatingView];
    //}
}

-(void)starRatingValueDidChange:(HCSStarRatingView*)starRatingView{
    //if([self.delegate respondsToSelector:@selector(starRatingValueDidChange:)]){
        [self.delegate starRatingValueDidChange:(HCSStarRatingView*)starRatingView];
    //}
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)switchSwitched:(UISwitch*)sender {
    if([self.delegate respondsToSelector:@selector(switchSwitched:)]){
        [self.delegate switchSwitched:sender];
    }
}

- (IBAction)sliderValueChanged:(id)sender {
    UISlider *slider = (UISlider*)sender;
    NSUInteger index = (NSUInteger)(slider.value + 0.5);
    [slider setValue:index animated:NO];
    if([self.delegate respondsToSelector:@selector(sliderValueChanged:)]){
        [self.delegate sliderValueChanged:(UISlider*)sender];
    }

}

-(NSString*)getStarRatingValueDescription:(int)value{
    if(_starValueDescriptions.count>value){
        return _starValueDescriptions[value];
    }
    return @"";
}


@end
