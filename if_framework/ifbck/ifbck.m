//
//  ifbck.m
//  nativeifbckapp
//
//  Created by User on 6/2/16.
//  Copyright © 2016 BHM Media Solutions. All rights reserved.
//

#import "OnboardingHelper.h"

@interface ifbck ()

@property NSInteger index;

@end

@implementation ifbck

+(ifbck*)sharedInstance{
    //  Recommended way according to Apple
    static ifbck* sharedInstance = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^
                  {
                      sharedInstance = [[ifbck alloc] init];
                  });
    
    return sharedInstance;
}

//+(ifbck*)new{
    //return [self sharedInstance];
//}

- (void)loadViewControllerForURL:(NSString *)url {
    iFeedbackModel *ifbckModel = [iFeedbackModel new];

    ifbckModel.delegate = self;
    ifbckModel.entryPoint = [url lastPathComponent];
    ifbckModel.entryURL = url;
    ifbckModel.navigationBarHeight = self.navigationBarHeight;
    [ifbckModel loadClient];
}

- (void)loadViewControllerForEntryPoint:(NSString*)entryPoint{
    iFeedbackModel *ifbckModel = [iFeedbackModel new];

    ifbckModel.delegate = self;
    ifbckModel.entryPoint = entryPoint;
    ifbckModel.navigationBarHeight = self.navigationBarHeight;
    [ifbckModel loadClient];
}

- (void)loadViewController{
    NSUserDefaults *standardDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *entryPoint = [[standardDefaults stringForKey:@"selectedEntryPoint"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@" ifbck - entryPoint: %@", entryPoint);
    if(entryPoint==nil || [entryPoint isEqualToString:@""]){
        NSLog(@" ifbck - no entrypoint");
        
        if([self.delegate respondsToSelector:@selector(ifbckDidLoadViewController:)]){
            ifbckOnboardingViewController *vc = (ifbckOnboardingViewController *)[OnboardingHelper getOnboardingViewControllerForIfbck:self];
            [self.delegate ifbckDidLoadViewController:vc];
        }
    }
    else {
        NSLog(@" ifbck - opening entrypoint");
        
        iFeedbackModel *ifbckModel = [iFeedbackModel new];
        
        ifbckModel.delegate = self;
        ifbckModel.entryPoint = entryPoint;
        ifbckModel.navigationBarHeight = self.navigationBarHeight;
        [ifbckModel loadClient];
    }
}

- (void)loadViewControllerForURL:(NSString*)url atIndex:(NSInteger)index{
    _index = index;

    [self loadViewControllerForURL:url];
}

-(void)iFeedbackModelDidLoadDataWithViewController:(UIViewController *)viewController forIndex:(NSUInteger)index{
    if([self.delegate respondsToSelector:@selector(ifbckDidLoadViewController:)]){
        [self.delegate ifbckDidLoadViewController:viewController];
    }
    if([self.delegate respondsToSelector:@selector(ifbckDidLoadViewController:atIndex:)]){
        [self.delegate ifbckDidLoadViewController:viewController atIndex:_index];
    }
}

-(UIViewController*)getOnboardingViewController{
    return [OnboardingHelper getOnboardingViewController];
}

-(void)loadIFeedbackViewController {
    iFeedbackModel *ifbckModel = [iFeedbackModel new];

    ifbckModel.delegate = self;
    ifbckModel.entryPoint = [[NSUserDefaults standardUserDefaults] stringForKey:@"selectedEntryPoint"];
    ifbckModel.navigationBarHeight = self.navigationBarHeight;
    [ifbckModel loadClient];
}

@end
