//
//  TopicListViewController.h
//  if_framework_terminal
//
//  Created by User on 4/18/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "iFeedbackViewController.h"

#import "TopicDetailViewController.h"


@protocol TopicListDelegate<NSObject>

@optional
- (void)averageRatingChanged:(float)averageRating;

@optional
- (void)numOfTopicsChanged:(int)numOfTopics;

@end

@interface TopicListViewController : iFeedbackViewController <UITableViewDelegate, UITableViewDataSource, TopicDetailDelegate, TopicListDelegate, UITabBarDelegate>

+(TopicListViewController*)newRootList;

// outlets
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIImageView *languageSelectorImageFlag;
@property (weak, nonatomic) IBOutlet UIButton *languageSelectorButton;


// properties
@property (nonatomic) float footerHeight;

@property (nonatomic) BOOL isRoot, hasNextSectionButton;
@property (nonatomic) NSInteger selectedRow;
@property (nonatomic) BOOL rowSelected;

@property (weak, nonatomic) IBOutlet UIView *headerView;

// data management
@property (nonatomic) NSMutableArray *topics;

@property (nonatomic) id<TopicListDelegate> delegate;

// public accessible functions
- (void)initRootViewController;

- (UIViewController *)getViewControllerForIndex:(NSInteger)index;

@end
