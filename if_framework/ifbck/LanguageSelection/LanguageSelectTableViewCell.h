//
//  LanguageSelectTableViewCell.h
//  if_framework_terminal
//
//  Created by User on 2/17/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "iFeedbackTableViewCell.h"

@interface LanguageSelectTableViewCell : iFeedbackTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelLanguageDescription;
@property (weak, nonatomic) IBOutlet UIImageView *imageLanguageFlag;

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headerImage;


@end