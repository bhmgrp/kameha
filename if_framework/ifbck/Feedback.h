//
//  Feedback.h
//  if_framework_terminal
//
//  Created by User on 4/14/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Feedback : NSManagedObject

- (void)log;

@end

NS_ASSUME_NONNULL_END

#import "Feedback+CoreDataProperties.h"
