//
//  OnboardingViewController.h
//  if_framework_terminal
//
//  Created by User on 4/20/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ifbckOnboardingViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *iconFrameView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *separatorHeight;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *templateImages;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *constraintsToShrinkBy3of4;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *constraintsToShrinkBy1of2;

- (void)registerForKeyboardNotifications;

@property (nonatomic) ifbck *ifbckModel;

@end
