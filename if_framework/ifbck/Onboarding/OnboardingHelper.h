//
//  OnboardingHelper.h
//  if_framework_terminal
//
//  Created by User on 4/25/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ifbckOnboardingNavigationViewController.h"
#import "ifbckOnboardingViewController.h"
#import "ifbckOnboardingSidebarViewController.h"
#import "ifbckLandingPageViewController.h"
#import "ifbckSelectConfigurationTypeViewController.h"
#import "ifbckInsertEntrypointViewController.h"
#import "ifbckScanEntrypointViewController.h"

@interface OnboardingHelper : NSObject

+(UIViewController*)getOnboardingViewController;

+(UIViewController*)getOnboardingViewControllerForIfbck:(ifbck*)ifbck;

@end
