//
//  ScanEntrypointViewController.m
//  if_framework_terminal
//
//  Created by User on 4/21/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "ifbckScanEntrypointViewController.h"

#import "ifbckWebViewController.h"

#import "ifbck.h"

@interface ifbckScanEntrypointViewController ()

@property (nonatomic) BOOL isReading,firstLayout;
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;

@property (nonatomic) NSString *entry;

@end

@implementation ifbckScanEntrypointViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *bar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 1.5f)];
    
    bar.backgroundColor = [UIColor colorWithRGBHex:0x4682BE];
    
    [_footerView addSubview:bar];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self stopReading];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if(!_firstLayout){
        if (!_isReading) {
            if ([self startReading]) {
                _isReading = YES;
            }
        }
        _firstLayout = YES;
    }
}

-(void)viewDidAppear:(BOOL)animated{
    if(_firstLayout){
        if (!_isReading) {
            if ([self startReading]) {
                _isReading = YES;
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)startReading {
    
    NSError *error;
    
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        return NO;
    }
    
    _captureSession = [[AVCaptureSession alloc] init];
    [_captureSession addInput:input];
    
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
    
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    captureMetadataOutput.metadataObjectTypes = @[AVMetadataObjectTypeQRCode];
    
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    _videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    _videoPreviewLayer.frame = _viewPreview.layer.bounds;
    [_viewPreview.layer addSublayer:_videoPreviewLayer];
    
    [_captureSession startRunning];
    
    return YES;
}

-(void)stopReading{
    [_captureSession stopRunning];
    _captureSession = nil;
    
    [_videoPreviewLayer removeFromSuperlayer];
    
}

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    if (metadataObjects != nil && metadataObjects.count > 0) {
        AVMetadataMachineReadableCodeObject *metadataObj = metadataObjects[0];
        if ([metadataObj.type isEqualToString:AVMetadataObjectTypeQRCode]) {
            
            NSString *entry = metadataObj.stringValue.lastPathComponent;;
            
            if(_isReading){
                _isReading = NO;
                [self performSelectorOnMainThread:@selector(showPopupForEntry:) withObject:entry waitUntilDone:NO];
            }
        }
        
    }
}

-(void)showPopupForEntry:(NSString*)entry{
    _entry = entry;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTable(@"Advice",@"ifbckLocalizable", @"")
                                                    message:NSLocalizedStringFromTable(@"If you would like to use this app in a public area, we suggest you to activate the \"guided access\".",@"ifbckLocalizable", @"")
                                                   delegate:self cancelButtonTitle:NSLocalizedStringFromTable(@"OK",@"ifbckLocalizable", @"") otherButtonTitles:NSLocalizedStringFromTable(@"More information", @"ifbckLocalizable", @""), nil];
    alert.cancelButtonIndex = 1;
    
    [alert show];
}


-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex != alertView.cancelButtonIndex){
        [self loadiFeedback:_entry];
    }
    else {
        ifbckWebViewController *wvc = [ifbckWebViewController getWebViewController];
        
        wvc.url = [NSURL URLWithString:NSLocalizedStringFromTable(@"GUIDED_ACCESS_STRING", @"ifbckLocalizable", @"")];
        wvc.titleString = NSLocalizedStringFromTable(@"Guided Access", @"ifbckLocalizable", @"");
        wvc.urlGiven = YES;
        
        UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(loadiFeedback)];
        
        wvc.navigationItem.rightBarButtonItem = doneBarButtonItem;
        
        [self.navigationController pushViewController:wvc animated:YES];
    }

}

-(void)loadiFeedback{
    [self loadiFeedback:_entry];
}

-(void)loadiFeedback:(NSString*)iFeedbackLink{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedStringFromTable(@"Loading",@"ifbckLocalizable", @"");
    
    
    NSUserDefaults *standardDefaults = [NSUserDefaults standardUserDefaults];
    
    [standardDefaults setObject:iFeedbackLink forKey:@"selectedEntryPoint"];
    
    [standardDefaults synchronize];
    
    [self.ifbckModel loadViewControllerForEntryPoint:iFeedbackLink];
}

@end
