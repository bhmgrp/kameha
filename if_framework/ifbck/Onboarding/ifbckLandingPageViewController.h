//
//  TerminalLandingPageViewController.h
//  if_framework_terminal
//
//  Created by User on 4/14/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ifbckOnboardingViewController.h"

@interface ifbckLandingPageViewController : ifbckOnboardingViewController


@property (weak, nonatomic) IBOutlet UIButton *moreButton;
@property (weak, nonatomic) IBOutlet UIButton *alreadyCustomerButton;
@property (weak, nonatomic) IBOutlet UIButton *registerNewAccountButton;
@property (weak, nonatomic) IBOutlet UILabel *pleaseSelectLabel;
@property (weak, nonatomic) IBOutlet UILabel *startConfigurationLabel;
@property (weak, nonatomic) IBOutlet UILabel *firstStepLabel;

@end
