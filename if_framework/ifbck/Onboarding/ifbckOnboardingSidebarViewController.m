//
//  OnboardingSidebarViewController.m
//  if_framework_terminal
//
//  Created by User on 4/25/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "ifbckOnboardingSidebarViewController.h"

#import "ifbckOnboardingNavigationViewController.h"
#import "ifbckWebViewController.h"
#import "ifbckLandingPageViewController.h"
#import "ifbckSelectConfigurationTypeViewController.h"

@interface ifbckOnboardingSidebarViewController ()

@property (nonatomic) NSArray *data;

enum{OPEN_START = 0, OPEN_WEB = 1, OPEN_GUIDED_ACCESS = 2, OPEN_IFEEDBACK_START = 3};

@end

@implementation ifbckOnboardingSidebarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    NSString *entryPoint = [[[NSUserDefaults standardUserDefaults] stringForKey:@"selectedEntryPoint"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if(entryPoint==nil || [entryPoint isEqualToString:@""]){
        _data = @[
                  @{
                      @"sectionTitle":@"Title",
                      @"sectionCells":
                          @[
                              @{
                                  @"title":NSLocalizedStringFromTable(@"Start", @"ifbckLocalizable", @""),
                                  @"open":@(OPEN_START),
                                  @"openString":@"",
                                  },
                              @{
                                  @"title":NSLocalizedStringFromTable(@"Send request",@"ifbckLocalizable", @""),
                                  @"open":@(OPEN_WEB),
                                  @"openString":NSLocalizedStringFromTable(@"SITE_IFEEDBACK.DE/CONTACT", @"ifbckLocalizable", @""),
                                  },
                              ],
                      
                      },
                  @{
                      @"sectionTitle":@"Title",
                      @"sectionCells":
                          @[
                              @{
                                  @"title":NSLocalizedStringFromTable(@"Lock your device", @"ifbckLocalizable", @""),
                                  @"open":@(OPEN_WEB),
                                  @"openString":NSLocalizedStringFromTable(@"GUIDED_ACCESS_STRING", @"ifbckLocalizable", @""),
                                  },
                              @{
                                  @"title":NSLocalizedStringFromTable(@"Imprint",@"ifbckLocalizable", @""),
                                  @"open":@(OPEN_WEB),
                                  @"openString":NSLocalizedStringFromTable(@"SITE_IFEEDBACK.DE/IMPRINT", @"ifbckLocalizable", @""),
                                  },
                              @{
                                  @"title":NSLocalizedStringFromTable(@"Privacy policy",@"ifbckLocalizable", @""),
                                  @"open":@(OPEN_WEB),
                                  @"openString":NSLocalizedStringFromTable(@"SITE_IFEEDBACK.DE/PRIVACYPOLICY", @"ifbckLocalizable", @""),
                                  },
                              ],
                      
                      },
                  ];
    }
    else {
        _data = @[
                  @{
                      @"sectionTitle":@"Title",
                      @"sectionCells":
                          @[
                              @{
                                  @"title":NSLocalizedStringFromTable(@"Start", @"ifbckLocalizable", @""),
                                  @"open":@(OPEN_IFEEDBACK_START),
                                  @"openString":@"",
                                  },
                              ],
                      
                      },
                  @{
                      @"sectionTitle":@"Title",
                      @"sectionCells":
                          @[
                              @{
                                  @"title":NSLocalizedStringFromTable(@"Imprint",@"ifbckLocalizable", @""),
                                  @"open":@(OPEN_WEB),
                                  @"openString":NSLocalizedStringFromTable(@"SITE_IFEEDBACK.DE/IMPRINT", @"ifbckLocalizable", @""),
                                  },
                              @{
                                  @"title":NSLocalizedStringFromTable(@"Privacy policy",@"ifbckLocalizable", @""),
                                  @"open":@(OPEN_WEB),
                                  @"openString":NSLocalizedStringFromTable(@"SITE_IFEEDBACK.DE/PRIVACYPOLICY", @"ifbckLocalizable", @""),
                                  },
                              ],
                      
                      },
                  ];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _data.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return ((NSArray*)_data[section][@"sectionCells"]).count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.textLabel.text = _data[indexPath.section][@"sectionCells"][indexPath.row][@"title"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSInteger open = [_data[indexPath.section][@"sectionCells"][indexPath.row][@"open"] integerValue];
    
    if(open == OPEN_START) {
        ifbckSelectConfigurationTypeViewController *lpvc = [ifbckSelectConfigurationTypeViewController loadNowFromStoryboard:@"ifbckSelectConfigurationTypeViewController"];
        
        [lpvc setSidebarButton];
        
        ifbckOnboardingNavigationViewController *onvc = [[ifbckOnboardingNavigationViewController alloc] initWithRootViewController:lpvc];
        
        [self.revealViewController pushFrontViewController:onvc animated:YES];
    }
    
    if(open == OPEN_IFEEDBACK_START) {
        
    }
    
    if(open == OPEN_WEB) {
        NSString *openString = _data[indexPath.section][@"sectionCells"][indexPath.row][@"openString"];
        if([_data[indexPath.section][@"sectionCells"][indexPath.row][@"openString"] hasPrefix:@"http"]){
            ifbckWebViewController *wvc = [ifbckWebViewController getWebViewController];
            
            wvc.titleString = [_data[indexPath.section][@"sectionCells"][indexPath.row][@"title"] uppercaseString];
            wvc.url = [NSURL URLWithString:openString];
            wvc.urlGiven = YES;
            
            [wvc setSidebarButton];
            
            ifbckOnboardingNavigationViewController *onvc = [[ifbckOnboardingNavigationViewController alloc] initWithRootViewController:wvc];
            
            [self.revealViewController pushFrontViewController:onvc animated:YES];
            
            return;
        }
        
        ifbckWebViewController *wvc = [ifbckWebViewController getWebViewController];
        
        wvc.titleString = [_data[indexPath.section][@"sectionCells"][indexPath.row][@"title"] uppercaseString];
        wvc.htmlString = openString;
        wvc.useHTML = YES;
        
        [wvc setSidebarButton];
        
        ifbckOnboardingNavigationViewController *onvc = [[ifbckOnboardingNavigationViewController alloc] initWithRootViewController:wvc];
        
        [self.revealViewController pushFrontViewController:onvc animated:YES];
        
        return;
    }
    
}

@end
