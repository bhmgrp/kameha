//
//  AppDelegate.h
//  pickalbatros
//
//  Created by User on 09.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "SidebarViewController.h"
#import <linkedin-sdk/LISDK.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@property (nonatomic) SidebarViewController *sbvc;


@property (strong, nonatomic) NSString *urlToOpen, *titleForWebsite;

@property (strong, nonatomic) NSData *deviceToken;

@property (strong, nonatomic) UIAlertView *notificationAlert;

@property (nonatomic) BOOL notificationCameIn;

@property (strong, nonatomic) NSDictionary *notificationData;


+(UIViewController*)getStartViewController;

@end