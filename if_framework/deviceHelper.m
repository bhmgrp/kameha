//
//  DeviceHelper.m
//  if_framework_terminal
//
//  Created by User on 4/12/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "deviceHelper.h"
#import "SSKeychain.h"

@implementation deviceHelper

+(void)logDeviceInformation{
    NSLog(@"Device: batteryLevel: %f", [self deviceBatteryLevel]);
    NSLog(@"Device: batteryState: %@", [self deviceBatteryState]);
    NSLog(@"Device: localizedModel: %@", [UIDevice currentDevice].localizedModel);
    NSLog(@"Device: model: %@", [UIDevice currentDevice].model);
    NSLog(@"Device: name: %@", [UIDevice currentDevice].name);
    NSLog(@"Device: systemName: %@", [UIDevice currentDevice].systemName);
    NSLog(@"Device: systemVersion: %@", [UIDevice currentDevice].systemVersion);
    NSLog(@"Device: UUID: %@", [self deviceUUID]);
}


+(NSString*)deviceBatteryState{
    switch([UIDevice currentDevice].batteryState){
        case UIDeviceBatteryStateFull:return@"full";
        case UIDeviceBatteryStateUnknown:return@"unkown";
        case UIDeviceBatteryStateCharging:return@"charging";
        case UIDeviceBatteryStateUnplugged:return@"unplugged";
        default:return@"";
    }
}

+(float)deviceBatteryLevel{
    if([UIDevice currentDevice].batteryMonitoringEnabled){
        return [UIDevice currentDevice].batteryLevel*100;
    }
    
    if([UIDevice currentDevice].batteryState == UIDeviceBatteryStateFull){
        return 100.0f;
    }
    
    return 0.0f;
}

+(NSString*)deviceUUID{
    
    // getting the unique key (if present ) from keychain , assuming "your app identifier" as a key
    NSString *retrieveuuid = [SSKeychain passwordForService:@"com.bhmms.ifeedback-terminal" account:@"user"];
    if (retrieveuuid != nil) {
        // if we have an uuid, return this
        return retrieveuuid;
    }
    else {
        // if this is the first time app lunching , create key for device
        NSString *uuid  = ([UIDevice currentDevice].identifierForVendor).UUIDString;
        // save newly created key to Keychain
        [SSKeychain setPassword:uuid forService:@"com.bhmms.ifeedback-terminal" account:@"user"];
        // this is the one time process
        
        return uuid;
    }
}

+(NSDictionary*)getDeviceInfo{
    return @{
             @"batteryLevel":@([self deviceBatteryLevel]),
             @"batteryState":[self deviceBatteryState],
             @"model":[UIDevice currentDevice].model,
             @"name":[UIDevice currentDevice].name,
             @"systemVersion":[UIDevice currentDevice].systemVersion,
             @"vendorUUID":[self deviceUUID],
             };
}

+(NSDictionary*)getDeviceSettings{
    NSMutableDictionary *settings = [@{} mutableCopy];
    
    [settings addObjectFromUserDefaults:@"selectedEntryPoint"];
    [settings addObjectFromUserDefaults:@"cronjobTimer"];
    [settings addObjectFromUserDefaults:@"appInTestingMode"];
    
    
    return settings;
}

+(void)synchronizeDevice{
    @try{
        NSError *error;
        
        NSDictionary *postDict = @{
                                   @"vendor_uuid":[self deviceUUID],
                                   @"update_frequency":@([ifbckGlobals sharedInstance].cronjob.timer.timeInterval),
                                   @"device_info":[self getDeviceInfo],
                                   @"device_settings":[self getDeviceSettings],
                                   @"device_type":@(0),
                                   };
        
        NSString *post = [postDict urlEncodedString];
        
        
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@terminal.php?sync",SECURED_API_URL]];
        
        NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)postData.length];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        request.URL = url;
        request.HTTPMethod = @"POST";
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        request.HTTPBody = postData;
        request.timeoutInterval = 240;
        
        
        NSHTTPURLResponse *response = nil;
        
        [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    }
    @catch(NSException *e){
    }
    
}

@end
