//
//  deviceTokenHandling.h
//  Freese Gruppe
//
//  Created by User on 04.12.14.
//  Copyright (c) 2014 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface deviceTokenHandling : NSObject

+(void)updateDeviceToken:(NSString*) token forUserID:(long) userID;
+(void)userLogoutForToken:(NSString*) token forUserID:(long) userID;
+(void)registerDeviceToken:(NSString*) token;
@end
