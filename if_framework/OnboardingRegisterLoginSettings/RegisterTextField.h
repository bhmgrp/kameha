//
//  RegisterTextField.h
//  pickalbatros
//
//  Created by User on 12.05.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterTextField : UITextField

@property (nonatomic) NSIndexPath *selectedIndexPath;
@property (nonatomic) BOOL dynamicSetting;
@end
