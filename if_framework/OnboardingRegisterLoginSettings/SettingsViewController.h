//
//  SettingsViewController.h
//  KAMEHA
//
//  Created by User on 23.02.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RegisterTableViewCell.h"
#import "RegisterTextField.h"

@interface SettingsViewController : UIViewController <
    UITableViewDataSource,
    UITableViewDelegate,
    UITextFieldDelegate,
    registerCellDelegate,
    UIPickerViewDataSource,
    UIPickerViewDelegate,
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate,
    UIActionSheetDelegate
>

@property (nonatomic) NSString *titleString;

- (IBAction)pickerViewDone:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *pickerViewFrame;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@property (weak, nonatomic) IBOutlet UILabel *settingType;

@property (nonatomic) NSString* profileImageFileName;
@property (nonatomic) NSString* profileImageFilePath;
@property (nonatomic) UIImage *uploadedImage;
@property (nonatomic) UIImage *uploadingImage;
@property (nonatomic) NSString *uploadImageLabelText;
@property (nonatomic) BOOL uploadImageActivityIndicatorHidden;


+ (BOOL)updateDynamicSettings:(NSArray*)dynamicSettingsArray forUserID:(int)userID validationPassword:(NSString*)password;
+ (BOOL)updateDynamicSettings:(NSArray*)dynamicSettingsArray forUserID:(int)userID validationPassword:(NSString*)password ignoreLocalUpdates:(BOOL)ignoreLocalUpdates;

@end
