//
// Created by Nicolas Tichy on 1/30/15.
// Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SidebarViewController.h"




@interface OnboardingViewController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource, UIAlertViewDelegate>


@property (nonatomic) SidebarViewController *sbvc;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UIButton *skipButton;


@property (weak, nonatomic) IBOutlet UIButton *fbButton;
@property (weak, nonatomic) IBOutlet UIButton *liButton;

- (IBAction)fbSignUpButtonClicked:(id)sender;

- (IBAction)liSignUpButtonClicked:(id)sender;

- (void) afterLogin;

@end