//
//  RegisterHeaderViewController.h
//  KAMEHA
//
//  Created by User on 19.02.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterHeaderViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *labelHeaderTitle;


@end
