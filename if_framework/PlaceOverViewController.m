//
//  PlaceOverViewController.m
//  pickalbatros
//
//  Created by User on 13.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "PlaceOverViewController.h"
#import "PlaceOverViewTableViewCell.h"
#import "StructureOverViewController.h"
#import "SidebarViewController.h"
#import "shareMenu.h"


@interface PlaceOverViewController ()<UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating>{
    imageMethods *images;
    NSString *documentsPath;
    NSMutableArray *_searchResults;
    NSMutableArray *_filteredResults;
}



@property (nonatomic) UIAlertView *placeSelectionAlert;
@property (nonatomic) localStorage* placeStorage;
@property (nonatomic) NSIndexPath *selectedIndexPath;
@property (nonatomic) NSArray* cellArray;


@property (nonatomic, strong) UISearchController *searchController;
// our secondary search results table view
@property (nonatomic, strong) UITableViewController *resultsTableController;

@end

@implementation PlaceOverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    images = [[imageMethods alloc] init];
    documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    self.placeStorage = [localStorage storageWithFilename:@"places"];
    
    self.navigationItem.titleView = [NavigationTitle createNavTitle:NSLocalizedString(@"Country",@"") SubTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"]];
    
    if(_showSidebarButton){
        self.navigationItem.leftBarButtonItem = self.sideBarButton;
        [self.sideBarButton setTarget: self.revealViewController];
        [self.sideBarButton setAction: @selector( revealToggle: )];
        [self.revealViewController setRearViewController:[SidebarViewController loadNowFromStoryboard:@"Sidebar"]];
        [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
    
    if(_showShareButton){
        self.navigationItem.rightBarButtonItem = _shareBarButtonItem;
    }
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    

    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedString(@"Loading",@"");
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [super viewWillAppear:animated];
    [self loadData];
    [self loadSearchBar];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        //[cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)prepareCellsArrayForTableView:(UITableView*)tableView{
    NSMutableArray *cellArray = [@[] mutableCopy];
    
    for(NSDictionary *cellDict in _dataArray){
        
        NSMutableDictionary *cellD = [@{} mutableCopy];
        
        NSString *cellIdentifier;
        
        if([cellDict[@"place_type"] intValue] == 0){
            cellIdentifier = @"placeGroupCell";
        }
        else {
            cellIdentifier = @"placeCell";
            
            //PlaceOverViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        }
        
        cellD[@"identifier"] = cellIdentifier;
        
        cellD[@"name"] = cellDict[@"name"];
        
        [cellArray addObject:cellD];
    }
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    self.cellArray = cellArray;
    [tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *cD = _dataArray[indexPath.row];
    
    if(tableView == self.resultsTableController.tableView){
        cD = _filteredResults[indexPath.row];
    }
    
    PlaceOverViewTableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:@"placeGroupCell"];
    
    cell.labelPlaceName.text = cD[@"name"];
    cell.labelPlaceName.layer.cornerRadius = 10.0f;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.resultsTableController.tableView){
        return 45;
    } return 45;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(tableView == self.resultsTableController.tableView){
        return _filteredResults.count;
    }
    return 0;
    //return _cellArray.count;
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *cellData = _dataArray[indexPath.row];
    
    NSLog(@"table view: %@", tableView);
    
    if (tableView == self.resultsTableController.tableView) {
        cellData = _filteredResults[indexPath.row];
    }
    
    if ([cellData[@"place_type"] intValue] == 0) {
        PlaceOverViewController *povc = [PlaceOverViewController loadNowFromStoryboard:@"PlaceOverView"];
        povc.reloadData = _reloadData;
        povc.showShareButton = YES;
        povc.placeID = [cellData[@"uid"] intValue];
        povc.title = cellData[@"name"];
        [self.navigationController pushViewController:povc animated:YES];
        
    } else {
        
        cellData = _filteredResults[indexPath.row];
        self.selectedIndexPath = indexPath;
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = NSLocalizedString(@"Loading",@"");
        
        NSLog(@"selected place: %@",cellData);
        
        [[NSUserDefaults standardUserDefaults] setInteger:[cellData[@"uid"] intValue] forKey:@"selectedPlaceID"];
        [[NSUserDefaults standardUserDefaults] setObject:cellData[@"name"] forKey:@"selectedPlaceName"];
        [[NSUserDefaults standardUserDefaults] setObject:cellData[@"placeBackgroundColor"] forKey:@"placeBackgroundColor"];
        [[NSUserDefaults standardUserDefaults] setObject:cellData[@"placeTextColor"] forKey:@"placeTextColor"];
        [[NSUserDefaults standardUserDefaults] setObject:cellData[@"tstamp"] forKey:@"placeVersionTimestamp"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[localStorage storageWithFilename:@"currentPlace"] setObject:cellData forKey:@"selectedPlaceDictionary"];
        
        [globals setPID:[cellData[@"pid"] integerValue]];
        [globals setPlaceID:[cellData[@"uid"] integerValue]];
        
        NSLog(@"updating dynamic settings");

        NSLog(@"dynamic settings updated, finished in this view");
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sidebarUpdate" object:nil];
        
        LoadingInformationViewController *livc = [LoadingInformationViewController loadNowFromStoryboard:@"LoadingInformation"];
        livc.delegate = self;
        livc.pID = [cellData[@"pid"] intValue];
        livc.skipUserInput = YES;
        livc.headLineText = NSLocalizedString(@"Download contents for the selected location?",@"");
        livc.providesPresentationContextTransitionStyle = YES;
        livc.definesPresentationContext = YES;
        [livc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
        SWRevealViewController *vc = ((SWRevealViewController*)[UIApplication sharedApplication].keyWindow.rootViewController);
        
        NSLog(@"root view controller: %@", [UIApplication sharedApplication].keyWindow.rootViewController);
        
        [livc showAsOverlayOnViewController:vc];
    }
}

- (void)downloadItems
{
    // Download the json file
    
    // Create the request
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:_jsonFileUrl cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:60.0];
    // Create the NSURLConnection
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

-(void) alertView:(UIAlertView *) alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (alertView == self.placeSelectionAlert && buttonIndex != 0) {
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = NSLocalizedString(@"Loading",@"");
        
        NSDictionary *cellData = _filteredResults[self.selectedIndexPath.row];
        [[NSUserDefaults standardUserDefaults] setInteger:[cellData[@"uid"] intValue] forKey:@"selectedPlaceID"];
        [[NSUserDefaults standardUserDefaults] setObject:cellData[@"name"] forKey:@"selectedPlaceName"];
        [[NSUserDefaults standardUserDefaults] setObject:cellData[@"placeBackgroundColor"] forKey:@"placeBackgroundColor"];
        [[NSUserDefaults standardUserDefaults] setObject:cellData[@"placeTextColor"] forKey:@"placeTextColor"];
        [[NSUserDefaults standardUserDefaults] setObject:cellData[@"tstamp"] forKey:@"placeVersionTimestamp"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[localStorage storageWithFilename:@"currentPlace"] setObject:cellData forKey:@"selectedPlaceDictionary"];
        int placeID = [cellData[@"uid"] intValue];
        
        OfferViewController *sovc = [OfferViewController loadNowFromStoryboard:@"OfferViewController"];

        sovc.placeID = SINGLEPLACEID;
        
        sovc.placeID = placeID;
        sovc.title = cellData[@"name"];
        
        [self.navigationController pushViewController:sovc animated:YES];
        
        NSLog(@"updating dynamic settings");
        
       /* // save to dynamic settings:
        NSMutableArray *dynamicSettingsArray = [@[
                                                  [@{@"type":@"inputCell",
                                                     @"inputTag":@0,
                                                     @"label":@"",
                                                     @"placeHolder":@"",
                                                     @"value":[NSNumber numberWithInt:[cellData[@"uid"] intValue]],
                                                     @"secureInput":@NO,
                                                     @"attributesTableID":@6,
                                                     @"dynamicSettings":@YES,
                                                     @"cellAccessory":[NSNumber numberWithInt:UITableViewCellAccessoryNone],
                                                     @"keyboardType":[NSNumber numberWithInt:UIKeyboardTypeAlphabet],
                                                     } mutableCopy],
                                                  ] mutableCopy];
        int userID = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"loggedInUserID"];
        NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedInUserName"];
        NSString *validationPassword = [SSKeychain passwordForService:@"PickalbatrosLogin" account:userName];
        
        [SettingsViewController updateDynamicSettings:dynamicSettingsArray forUserID:userID validationPassword:validationPassword];
        NSLog(@"dynamic settings updated, finished in this view");*/
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sidebarUpdate" object:nil];
    }
}

#pragma mark NSURLConnectionDataProtocol Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    // Parse the JSON that came in
    NSError *error;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingAllowFragments error:&error];
    
    NSLog(@"%@",jsonArray);
    
    _dataArray = jsonArray;
    
    for(NSDictionary* dict in jsonArray){
        [_searchResults addObject:dict];
    }
    //NSLog(@"%@", jsonArray);
    //NSLog(@"%@", jsonArray[0][@"name"]);
    [self prepareCellsArrayForTableView:self.tableView];
    
    [self.placeStorage setObject:jsonArray forKey:[NSString stringWithFormat:@"placeListForPlace%i",_placeID ]];
    [self.placeStorage setObject:jsonArray forKey:@"placeListArray"];
    
    // Loop through Json objects, create question objects and add them to our questions array
    
    //NSLog(@"jsonArray: %@", jsonArray);
//    int numOfElements = (int)[jsonArray count];
//    int i = 0;
    
    for (NSDictionary *jsonElement in jsonArray){
        //[_filteredResults addObject:jsonElement];
        if(jsonElement[@"checkout_elements"] != nil && jsonElement[@"checkout_elements"] != NULL && ![jsonElement[@"checkout_elements"] isKindOfClass:[NSNull class]]){
            if([jsonElement[@"checkout_elements"] count] > 0){
                [[localStorage storageWithFilename:@"checkout"] setObject:jsonElement[@"checkout_elements"] forKey:[NSString stringWithFormat:@"checkoutElementsForPlace%i",[jsonElement[@"uid"] intValue]]];
                if([jsonElement[@"uid"] intValue] == [[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceID"] intValue]){
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"sidebarUpdate" object:nil];
                }
            }
        }
    }
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSLog(@"Connection failed, Error: %@" ,error);
    [[[UIAlertView alloc] initWithTitle:nil message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}

-(void)loadData{
    _searchResults = [@[] mutableCopy];
    _filteredResults = [@[] mutableCopy];
    
    NSArray *tempArray = (NSArray*)[self.placeStorage objectForKey:[NSString stringWithFormat:@"placeListForPlace%i",_placeID ]];
    if(tempArray == nil || tempArray.count == 0 || _reloadData){
        NSString *urlString = [NSString stringWithFormat:@"%@get.php?getAllPlacesWithStructures&language=%i", API_URL, [languages currentLanguageID]];
        // Set the URL where we load from
        
        NSLog(@"%@", urlString);
        
        _jsonFileUrl = [NSURL URLWithString:urlString];
        
        [self downloadItems];
        
    } else {
        self.dataArray = tempArray;
        for(NSDictionary* dict in tempArray){
            [_searchResults addObject:dict];
        }
        [self prepareCellsArrayForTableView:self.tableView];
    }
}

-(void)hideLoadingInformationView{
    /*
     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
     hud.mode = MBProgressHUDModeIndeterminate;
     hud.labelText = NSLocalizedString(@"Downloading information",@"");*/
    
    NSDictionary *cellData = _filteredResults[self.selectedIndexPath.row];
    
    [[localStorage storageWithFilename:@"checkout"] setObject:cellData[@"checkout_sections"] forKey:@"checkout_elements"];
    int placeID = [cellData[@"uid"] intValue];
    NSLog(@"saved local information (place name, place id, text and background color ... starting to open structure overview");
    OfferViewController *sovc = [OfferViewController loadNowFromStoryboard:@"OfferViewController"];
    //            StructureOverViewController *sovc = [StructureOverViewController loadNowFromStoryboard:@"StructureOverView"];
    //            if(IDIOM == IPAD){
    //                sovc = [StructureOverViewController loadNowFromStoryboard:@"StructureOverView-iPad"];
    //            }
    //            sovc.showSidebarButton = YES;
    sovc.placeID = SINGLEPLACEID;
    sovc.placeID = placeID;
    sovc.title = cellData[@"name"];
    //stovc.imageForBackground = _cellArray[_selectedIndexPath.row][@"image"];
    //[self.navigationController pushViewController:sovc animated:YES];
    
    
//    OfferViewController *sovc = [OfferViewController loadNowFromStoryboard:@"OfferViewController"];
//    //            StructureOverViewController *sovc = [StructureOverViewController loadNowFromStoryboard:@"StructureOverView"];
//    //            if(IDIOM == IPAD){
//    //                sovc = [StructureOverViewController loadNowFromStoryboard:@"StructureOverView-iPad"];
//    //            }
//    //            sovc.showSidebarButton = YES;
//    sovc.placeID = [[[NSUserDefaults standardUserDefaults] valueForKey:@"selectedPlaceID"] intValue];
//    
//    
//    NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:sovc];
//    
//    SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:[SidebarViewController loadNowFromStoryboard:@"Sidebar"] frontViewController:nvc];
//    
//    
//    rvc.modalPresentationStyle = UIModalPresentationFullScreen;
//    rvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    
//    [self presentViewController:rvc animated:YES completion:^(void){
//        [UIApplication sharedApplication].keyWindow.rootViewController = rvc;
//        //[sovc setSideBarGestureFor:rvc];
//    }];
    

}

- (IBAction)shareButtonPressed:(id)sender {
    UIBarButtonItem *_sender = (UIBarButtonItem*)sender;
    [shareMenu shareDialogueInViewController:self fromSourceView:(UIView*)_sender];
}


#pragma mark - UISearchControllerDelegate

// Called after the search controller's search bar has agreed to begin editing or when
// 'active' is set to YES.
// If you choose not to present the controller yourself or do not implement this method,
// a default presentation is performed on your behalf.
//
// Implement this method if the default presentation is not adequate for your purposes.
//
- (void)presentSearchController:(UISearchController *)searchController {
    
}

- (void)willPresentSearchController:(UISearchController *)searchController {
    // do something before the search controller is presented
}

- (void)didPresentSearchController:(UISearchController *)searchController {
    // do something after the search controller is presented
}

- (void)willDismissSearchController:(UISearchController *)searchController {
    // do something before the search controller is dismissed
}

- (void)didDismissSearchController:(UISearchController *)searchController {
    // do something after the search controller is dismissed
}


-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    //[self.navigationController popViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:^(void){}];
    if([self.delegate respondsToSelector:@selector(searchCanceled)]){
        [self.delegate searchCanceled];
    }

}


#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    // update the filtered array based on the search text
    NSString *searchText = searchController.searchBar.text;
    NSMutableArray *searchResults = [_searchResults mutableCopy];
    
    // strip out all the leading and trailing spaces
    NSString *strippedString = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    // break up the search terms (separated by spaces)
    NSArray *searchItems = nil;
    if (strippedString.length > 0) {
        searchItems = [strippedString componentsSeparatedByString:@" "];
    }
    
    // build all the "AND" expressions for each value in the searchString
    //
    NSMutableArray *andMatchPredicates = [NSMutableArray array];
    
    for (NSString *searchString in searchItems) {
        // each searchString creates an OR predicate for: name, yearIntroduced, introPrice
        //
        // example if searchItems contains "iphone 599 2007":
        //      name CONTAINS[c] "iphone"
        //      name CONTAINS[c] "599", yearIntroduced ==[c] 599, introPrice ==[c] 599
        //      name CONTAINS[c] "2007", yearIntroduced ==[c] 2007, introPrice ==[c] 2007
        //
        NSMutableArray *searchItemsPredicate = [NSMutableArray array];
        
        // Below we use NSExpression represent expressions in our predicates.
        // NSPredicate is made up of smaller, atomic parts: two NSExpressions (a left-hand value and a right-hand value)
        
        // name field matching
        NSExpression *lhs = [NSExpression expressionForKeyPath:@"name"];
        NSExpression *rhs = [NSExpression expressionForConstantValue:searchString];
        NSPredicate *finalPredicate = [NSComparisonPredicate
                                       predicateWithLeftExpression:lhs
                                       rightExpression:rhs
                                       modifier:NSDirectPredicateModifier
                                       type:NSContainsPredicateOperatorType
                                       options:NSCaseInsensitivePredicateOption];
        [searchItemsPredicate addObject:finalPredicate];
        
        lhs = [NSExpression expressionForKeyPath:@"text1"];
        rhs = [NSExpression expressionForConstantValue:searchString];
        finalPredicate = [NSComparisonPredicate
                          predicateWithLeftExpression:lhs
                          rightExpression:rhs
                          modifier:NSDirectPredicateModifier
                          type:NSContainsPredicateOperatorType
                          options:NSCaseInsensitivePredicateOption];
        [searchItemsPredicate addObject:finalPredicate];
        
        // at this OR predicate to our master AND predicate
        NSCompoundPredicate *orMatchPredicates = [NSCompoundPredicate orPredicateWithSubpredicates:searchItemsPredicate];
        [andMatchPredicates addObject:orMatchPredicates];
    }
    
    // match up the fields of the Product object
    NSCompoundPredicate *finalCompoundPredicate =
    [NSCompoundPredicate andPredicateWithSubpredicates:andMatchPredicates];
    searchResults = [[searchResults filteredArrayUsingPredicate:finalCompoundPredicate] mutableCopy];
    
    // hand over the filtered results to our search results table
    UITableViewController *tableController = (UITableViewController*)self.searchController.searchResultsController;
    //tableController.filteredProducts = searchResults;
    _filteredResults = searchResults;
    [tableController.tableView reloadData];
    
}

-(void)loadSearchBar{
    
    _resultsTableController = [[UITableViewController alloc]init];
    _resultsTableController.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _searchController = [[UISearchController alloc] initWithSearchResultsController:self.resultsTableController];
    self.searchController.searchResultsUpdater = self;
    self.searchController.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation = YES; // default is YES
    self.searchController.searchBar.delegate = self; // so we can monitor text changes + others
    [self.searchController.searchBar setTintColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
    [self.searchController.searchBar sizeToFit];

    self.searchController.searchBar.searchBarStyle = UISearchBarStyleDefault;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    // we want to be the delegate for our filtered table so didSelectRowAtIndexPath is called for both tables
    self.resultsTableController.tableView.delegate = self;
    self.resultsTableController.tableView.dataSource = self;
    
    // Search is now just presenting a view controller. As such, normal view controller
    // presentation semantics apply. Namely that presentation will walk up the view controller
    // hierarchy until it finds the root view controller or one that defines a presentation context.
    //
    self.definesPresentationContext = YES;  // know where you want UISearchController to be displayed
    
    [self.searchController setActive:YES];
    //[self.searchController.searchBar becomeFirstResponder];
    //[self.searchController.searchBar becomeFirstResponder];

}


@end
