//
//  shareMenu.h
//  if_framework
//
//  Created by User on 30.06.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface shareMenu : NSObject

+(void)shareDialogueInViewController:(UIViewController*)currentViewController fromSourceView:(UIView*)sourceView;
@end
