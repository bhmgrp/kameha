//
//  StartTabBarViewController.h
//  if_framework
//
//  Created by User on 3/4/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "iFeedbackModel.h"

@interface StartTabBarViewController : UITabBarController <ifbckDelegate, UINavigationControllerDelegate>

@end
