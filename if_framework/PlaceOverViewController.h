//
//  PlaceOverViewController.h
//  pickalbatros
//
//  Created by User on 13.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoadingInformationViewController.h"

@protocol PlaceOverViewDelegate <NSObject>

-(void)searchCanceled;


@end

@interface PlaceOverViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NSURLConnectionDelegate, UIAlertViewDelegate, loadingInformationProtocol, UISearchControllerDelegate>

@property (nonatomic) id<PlaceOverViewDelegate> delegate;

@property (nonatomic) BOOL showSidebarButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideBarButton;

@property (nonatomic, strong) NSMutableData *downloadedData;
@property (nonatomic) BOOL reloadData;
@property (nonatomic, strong) NSURL *jsonFileUrl;

@property (nonatomic) BOOL showShareButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *shareBarButtonItem;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIImageView *shareButtonImage;
- (IBAction)shareButtonPressed:(id)sender;

@property (nonatomic) int placeID;

@property (nonatomic) NSArray *dataArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (void)downloadItems;

@property (nonatomic) BOOL ignoreImages;

-(void)loadData;

@end
