//
//  NavigationTitle.h
//  if_framework
//
//  Created by Julian Böhnke on 31.03.16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NavigationTitle : NSObject

+ (UIView*) createNavTitle:(NSString *)title SubTitle:(NSString *)subTitle;

@end
