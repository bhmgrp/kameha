//
//  GuestGuideStartViewController.m
//  if_framework
//
//  Created by User on 11/4/15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "gestgidStartViewController.h"
#import "NavigationViewController.h"
#import "LoadingInformationViewController.h"
#import "SettingsViewController.h"
#import "SSKeychain.h"
#import "AppDelegate.h"

#import "WebViewController.h"

#import "gestgidTableViewCell.h"

#import "StartTabBarViewController.h"

#import "MBProgressHUD.h"

@interface gestgidStartViewController () <loadingInformationProtocol>

@property (nonatomic) NSURL *jsonFileUrl;
@property (nonatomic, strong) NSMutableData *downloadedData;

@property (nonatomic) NSMutableArray *searchResults;

@property (nonatomic) NSMutableArray *filteredResults;


@property (nonatomic) localStorage* placeStorage;

@property (nonatomic) NSIndexPath *selectedIndexPath;

@end

@implementation gestgidStartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    UIColor *searchTintColor = [UIColor colorWithRGBHex:0x303030];
    
    UITapGestureRecognizer *tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(startSearch)];

    _searchFrameView.layer.borderWidth = 1.5f;
    _searchFrameView.layer.borderColor = [searchTintColor CGColor];
    _searchFrameViewFrame = CGRectMake(_searchFrameView.x, _searchFrameView.y, _searchFrameView.width, _searchFrameView.height);
    
    _searchLabel.tintColor = searchTintColor;
    
    [_searchFrameView addGestureRecognizer:tab];
    
    _searchIcon.tintColor = searchTintColor;
    
    [self loadData];
    [self loadSearchBar];
    
    _registerButton.layer.cornerRadius = 5.0f;
    _registerButton.layer.borderWidth = 1.5f;
    _registerButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    _headerImageView.image = [_headerImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _headerImageView.tintColor = [UIColor whiteColor];

    _placeStorage = [localStorage storageWithFilename:@"places"];
    
    _tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);;
    _tableView.separatorInset = UIEdgeInsetsZero;
    
    [self registerForKeyboardNotifications];
    
    [self localizeOutlets];
}

- (void)localizeOutlets{
    _searchLabel.text = NSLocalizedString(@"Find your host ...", @"");
    
    _areYouHotelierLabel.text = NSLocalizedString(@"Are you a host?", @"");
    
    [_registerButton setTitle:NSLocalizedString(@"register for free", @"") forState:UIControlStateNormal];
    
    NSDictionary *attributes = @{
                                 NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:14.0f],
                                 };
    
    NSMutableAttributedString *isAServiceOfText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"gestgid %@ ",NSLocalizedString(@"is a service of the", @"")] attributes:attributes];
    
    attributes = @{
                   NSForegroundColorAttributeName: [UIColor whiteColor],
                   NSFontAttributeName: [UIFont fontWithName:@"Helvetica-bold" size:14.0f],
                   };
    
    [isAServiceOfText appendAttributedString:[[NSAttributedString alloc] initWithString:@"BHM GROUP" attributes:attributes]];
    
    _gestgidIsAServiceLabel.attributedText = isAServiceOfText;
    if([UIScreen mainScreen].bounds.size.height < 568){
        _gestgidIsAServiceLabel.attributedText = nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NavigationViewController *nvc = (NavigationViewController*)self.navigationController;
    
    [nvc fadeNavigationBarBackground:YES animated:YES finished:^(void){
        [nvc setNavigationBarHidden:YES animated:YES];
    }];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];}

- (void)viewDidLayoutSubviews{
    _searchFrameViewFrame = CGRectMake(_searchViewWrapper.x, _searchViewWrapper.y, _searchViewWrapper.width, _searchViewWrapper.height);
}


- (void)loadSearchBar{
    
    _resultsTableController = [[UITableViewController alloc]init];
    _resultsTableController.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _searchController = [[UISearchController alloc] initWithSearchResultsController:self.resultsTableController];
    
    _searchController.searchResultsUpdater = self;
    _searchController.delegate = self;
    _searchController.dimsBackgroundDuringPresentation = NO; // default is YES
    _searchController.view.backgroundColor = [UIColor clearColor];
    
    _searchController.searchBar.delegate = self; // so we can monitor text changes + others
    
    _searchController.searchBar.searchBarStyle = UISearchBarStyleDefault;
    _searchController.searchBar.tintColor = [UIColor whiteColor];
    _searchController.searchBar.barTintColor = [UIColor colorWithRGBHex:0x303030];
    _searchController.searchBar.layer.borderWidth = 1.0f;
    _searchController.searchBar.layer.borderColor = [[UIColor colorWithRGBHex:0x303030] CGColor];
    _searchController.searchBar.backgroundImage = [UIImage new];
    _searchController.searchBar.keyboardAppearance = UIKeyboardAppearanceDark;
    _searchController.searchBar.showsCancelButton = NO;
    _searchController.searchBar.showsScopeBar = NO;
    _searchController.searchBar.placeholder = NSLocalizedString(@"Find your host ...", @"");
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTintColor:[UIColor colorWithRGBHex:0x303030]];
    
    _tableView.tableHeaderView = _searchController.searchBar;
    
    [_searchController didMoveToParentViewController:self];
    
    // we want to be the delegate for our filtered table so didSelectRowAtIndexPath is called for both tables
    _resultsTableController.tableView.delegate = self;
    _resultsTableController.tableView.dataSource = self;
    _resultsTableController.tableView.backgroundColor = [UIColor clearColor];
    _resultsTableController.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);;
    _resultsTableController.tableView.separatorInset = UIEdgeInsetsZero;
    _resultsTableController.tableView.separatorColor = [UIColor whiteColor];

    // Search is now just presenting a view controller. As such, normal view controller
    // presentation semantics apply. Namely that presentation will walk up the view controller
    // hierarchy until it finds the root view controller or one that defines a presentation context.
    _searchController.definesPresentationContext = YES;
}

- (void)startSearch{
    
    _searchBackground.alpha = 0.0f;
    _searchBackground.frame = CGRectMake(_searchFrameView.x, _searchFrameView.y, _searchFrameView.width, _searchFrameView.height);
    _tableView.alpha = 0.0f;
    _tableView.frame = CGRectMake(_searchFrameView.x, _searchFrameView.y, _searchFrameView.width, _searchFrameView.height);
    
    
    [self.view addSubview:_searchBackground];
    [self.view addSubview:_tableView];
    
    
    [UIView animateWithDuration:0.3 animations:^(void){
        _searchBackground.alpha = 1.0f;
        _searchBackground.frame = CGRectMake(0,0,self.view.width,self.view.height);
        _tableView.alpha = 1.0f;
        _tableView.frame = CGRectMake(0,0,self.view.width,self.view.height);
    } completion:^(BOOL finished){
        [_searchController.searchBar becomeFirstResponder];
    }];
    
}

- (void)hideSearchTable{
    
    [UIView animateWithDuration:0.5 animations:^(void){
        _searchBackground.alpha = 0.0f;
        _tableView.alpha = 0.0f;
    } completion:^(BOOL finished){
        [_searchBackground removeFromSuperview];
        [_tableView removeFromSuperview];
    }];
    
}


#pragma mark NSURLConnectionDataProtocol Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    // Parse the JSON that came in
    NSError *error;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingAllowFragments error:&error];
        
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    for(NSDictionary* dict in jsonArray){
        [_searchResults addObject:dict];
    }
    
    [_placeStorage setObject:jsonArray forKey:[NSString stringWithFormat:@"placeListForPlace%i",0 ]];
    [_placeStorage setObject:jsonArray forKey:@"placeListArray"];
    
    // Loop through Json objects, create question objects and add them to our questions array
    
    for (NSDictionary *jsonElement in jsonArray){
        if(jsonElement[@"checkout_elements"] != nil && jsonElement[@"checkout_elements"] != NULL && ![jsonElement[@"checkout_elements"] isKindOfClass:[NSNull class]]){
            if([jsonElement[@"checkout_elements"] count] > 0){
                [[localStorage storageWithFilename:@"checkout"] setObject:jsonElement[@"checkout_elements"] forKey:[NSString stringWithFormat:@"checkoutElementsForPlace%i",[jsonElement[@"uid"] intValue]]];
                if([jsonElement[@"uid"] intValue] == [[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceID"] intValue]){
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"sidebarUpdate" object:nil];
                }
            }
        }
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    NSLog(@"Connection failed, Error: %@" ,error);
    [[[UIAlertView alloc] initWithTitle:nil message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}

- (void)loadData{

    _searchResults = [@[] mutableCopy];
    _filteredResults = [@[] mutableCopy];
    

        NSString *urlString = [NSString stringWithFormat:
                               @"%@get.php?getAllPlacesWithStructures&language=%i%@",
                               API_URL,
                               [languages currentLanguageID],
                               @""];
    // Set the URL where we load from
        
        NSLog(@"%@", urlString);
        
        _jsonFileUrl = [NSURL URLWithString:urlString];
        
        [self downloadItems];
}

- (void)downloadItems{
    // Download the json file
    
    // Create the request
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:_jsonFileUrl cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:60.0];
    // Create the NSURLConnection
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}


#pragma mark table view delegate and datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _filteredResults.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    gestgidTableViewCell *c = [self.tableView dequeueReusableCellWithIdentifier:@"placeCell"];
    NSDictionary *placeDict = _filteredResults[indexPath.row];
    
    c.labelPlaceName.text = placeDict[@"name"];
    
    NSString *placeAddressString = [NSString stringWithFormat:@"%@, %@",placeDict[@"place_city"],placeDict[@"place_country"]];
    
    c.labelPlaceAddress.text = placeAddressString;
    
    return c;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *cellData = _filteredResults[indexPath.row];
    
    self.selectedIndexPath = indexPath;
    [[NSUserDefaults standardUserDefaults] setInteger:[cellData[@"uid"] intValue] forKey:@"selectedPlaceID"];
    [[NSUserDefaults standardUserDefaults] setObject:cellData[@"name"] forKey:@"selectedPlaceName"];
    [[NSUserDefaults standardUserDefaults] setObject:cellData[@"placeBackgroundColor"] forKey:@"placeBackgroundColor"];
    [[NSUserDefaults standardUserDefaults] setObject:cellData[@"placeTextColor"] forKey:@"placeTextColor"];
    [[NSUserDefaults standardUserDefaults] setObject:cellData[@"tstamp"] forKey:@"placeVersionTimestamp"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[localStorage storageWithFilename:@"currentPlace"] setObject:cellData forKey:@"selectedPlaceDictionary"];
    
    [[localStorage storageWithFilename:@"checkout"] setObject:cellData[@"checkout_sections"] forKey:@"checkout_elements"];
    
    [[globals sharedInstance] setPID:[cellData[@"pid"] integerValue]];
    [[globals sharedInstance] setPlaceID:[cellData[@"uid"] integerValue]];
    
    NSLog(@"updating dynamic settings");
    
    //save to dynamic settings:
    NSMutableArray *dynamicSettingsArray = [@[
                                                      [@{@"type":@"inputCell",
                                                         @"inputTag":@0,
                                                         @"label":@"",
                                                         @"placeHolder":@"",
                                                         @"value":[NSNumber numberWithInt:[cellData[@"uid"] intValue]],
                                                         @"secureInput":@NO,
                                                         @"attributesTableID":@6,
                                                         @"dynamicSettings":@YES,
                                                         @"cellAccessory":[NSNumber numberWithInt:UITableViewCellAccessoryNone],
                                                         @"keyboardType":[NSNumber numberWithInt:UIKeyboardTypeAlphabet],
                                                         } mutableCopy],
                                                      ] mutableCopy];
    int userID = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"loggedInUserID"];
    NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedInUserName"];
    NSString *validationPassword = [SSKeychain passwordForService:@"Pickalba trosLogin" account:userName];
    
    [SettingsViewController updateDynamicSettings:dynamicSettingsArray forUserID:userID validationPassword:validationPassword ignoreLocalUpdates:YES];
    
    NSLog(@"dynamic settings updated, finished in this view");
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sidebarUpdate" object:nil];
    
    LoadingInformationViewController *livc = [LoadingInformationViewController loadNowFromStoryboard:@"LoadingInformation"];
    livc.delegate = self;
    livc.pID = [cellData[@"pid"] intValue];
    livc.placeID = [cellData[@"uid"] intValue];
    livc.skipUserInput = YES;
    livc.headLineText = NSLocalizedString(@"Download contents for the selected location?",@"");
    livc.providesPresentationContextTransitionStyle = YES;
    livc.definesPresentationContext = YES;
    [livc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    
    [self hideSearchTable];
    
    [_searchController dismissViewControllerAnimated:NO completion:^(void){
        [self presentViewController:livc animated:NO completion:^{}];
    }];
    
}


#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    
    // update the filtered array based on the search text
    NSString *searchText = searchController.searchBar.text;
    NSMutableArray *searchResults = [_searchResults mutableCopy];
    
    // strip out all the leading and trailing spaces
    NSString *strippedString = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    // break up the search terms (separated by spaces)
    NSArray *searchItems = nil;
    if (strippedString.length > 0) {
        searchItems = [strippedString componentsSeparatedByString:@" "];
    }
    
    // build all the "AND" expressions for each value in the searchString
    //
    NSMutableArray *andMatchPredicates = [NSMutableArray array];
    
    for (NSString *searchString in searchItems) {
        // each searchString creates an OR predicate for: name, yearIntroduced, introPrice
        //
        // example if searchItems contains "iphone 599 2007":
        //      name CONTAINS[c] "iphone"
        //      name CONTAINS[c] "599", yearIntroduced ==[c] 599, introPrice ==[c] 599
        //      name CONTAINS[c] "2007", yearIntroduced ==[c] 2007, introPrice ==[c] 2007
        //
        NSMutableArray *searchItemsPredicate = [NSMutableArray array];
        
        // Below we use NSExpression represent expressions in our predicates.
        // NSPredicate is made up of smaller, atomic parts: two NSExpressions (a left-hand value and a right-hand value)
        
        // name field matching
        NSExpression *lhs = [NSExpression expressionForKeyPath:@"name"];
        NSExpression *rhs = [NSExpression expressionForConstantValue:searchString];
        NSPredicate *finalPredicate = [NSComparisonPredicate
                                       predicateWithLeftExpression:lhs
                                       rightExpression:rhs
                                       modifier:NSDirectPredicateModifier
                                       type:NSContainsPredicateOperatorType
                                       options:NSCaseInsensitivePredicateOption];
        [searchItemsPredicate addObject:finalPredicate];
        
        // city matching
        lhs = [NSExpression expressionForKeyPath:@"place_city"];
        rhs = [NSExpression expressionForConstantValue:searchString];
        finalPredicate = [NSComparisonPredicate
                          predicateWithLeftExpression:lhs
                          rightExpression:rhs
                          modifier:NSDirectPredicateModifier
                          type:NSContainsPredicateOperatorType
                          options:NSCaseInsensitivePredicateOption];
        [searchItemsPredicate addObject:finalPredicate];
        
        // country matching
        lhs = [NSExpression expressionForKeyPath:@"place_country"];
        rhs = [NSExpression expressionForConstantValue:searchString];
        finalPredicate = [NSComparisonPredicate
                          predicateWithLeftExpression:lhs
                          rightExpression:rhs
                          modifier:NSDirectPredicateModifier
                          type:NSContainsPredicateOperatorType
                          options:NSCaseInsensitivePredicateOption];
        [searchItemsPredicate addObject:finalPredicate];
        
        // country matching
        lhs = [NSExpression expressionForKeyPath:@"place_country_local"];
        rhs = [NSExpression expressionForConstantValue:searchString];
        finalPredicate = [NSComparisonPredicate
                          predicateWithLeftExpression:lhs
                          rightExpression:rhs
                          modifier:NSDirectPredicateModifier
                          type:NSContainsPredicateOperatorType
                          options:NSCaseInsensitivePredicateOption];
        [searchItemsPredicate addObject:finalPredicate];
        
        
        // at this OR predicate to our master AND predicate
        NSCompoundPredicate *orMatchPredicates = [NSCompoundPredicate orPredicateWithSubpredicates:searchItemsPredicate];
        [andMatchPredicates addObject:orMatchPredicates];
    }
    
    // match up the fields of the Product object
    NSCompoundPredicate *finalCompoundPredicate =
    [NSCompoundPredicate andPredicateWithSubpredicates:andMatchPredicates];
    searchResults = [[searchResults filteredArrayUsingPredicate:finalCompoundPredicate] mutableCopy];
    
    // hand over the filtered results to our search results table
    UITableViewController *tableController = (UITableViewController*)self.searchController.searchResultsController;
    //tableController.filteredProducts = searchResults;
    _filteredResults = [searchResults mutableCopy];
    [tableController.tableView reloadData];
    
}


#pragma mark - UISearchControllerDelegate

- (void)presentSearchController:(UISearchController *)searchController {
    
}

- (void)willPresentSearchController:(UISearchController *)searchController {
    
}

- (void)didPresentSearchController:(UISearchController *)searchController {
    
}

- (void)willDismissSearchController:(UISearchController *)searchController {
    
}

- (void)didDismissSearchController:(UISearchController *)searchController {
    [self hideSearchTable];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    //[self hideSearchTable];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    //[self hideSearchTable];
}


#pragma mark - Keyboard Notifications (for resizing view when keyboard appears)

- (void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
}

- (void)keyboardWillBeShown:(NSNotification*)aNotification{
    NSDictionary* info = [aNotification userInfo];
    
    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //self.view.height = [UIScreen mainScreen].bounds.size.height - (kbSize.height) + 60;
    
    self.navigationController.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - (kbSize.height));
    
    
    //self.headerView.height = 0;
    [self.view layoutIfNeeded];
}

- (void)keyboardWasShown:(NSNotification*)aNotification{

}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    self.navigationController.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    //self.view.height = [UIScreen mainScreen].bounds.size.height;
    [self.view layoutIfNeeded];
}


#pragma mark LoadingInformationProtocoll delegate methods

- (void)hideLoadingInformationView{
    
    NSLog(@"moving to start page");
    
    StartTabBarViewController *tabController = [StartTabBarViewController new];
    
    SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:[SidebarViewController loadNowFromStoryboard:@"Sidebar"] frontViewController:tabController];
    
    rvc.modalPresentationStyle = UIModalPresentationFullScreen;
    rvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self presentViewController:rvc animated:YES completion:^{
        [UIApplication sharedApplication].keyWindow.rootViewController = rvc;
    }];
}

- (IBAction)registerButtonClicked:(id)sender {
    WebViewController *wvc = [WebViewController loadNowFromStoryboard:@"WebView"];
    wvc.titleString = @"gestgid";
    wvc.url = [NSURL URLWithString:NSLocalizedString(@"https://ifbck.com/admin/user/registration-gestgid",@"")];
    wvc.urlGiven = YES;
    [self.navigationController pushViewController:wvc animated:YES];
}


@end