//
//  gestgidTableViewCell.h
//  if_framework
//
//  Created by User on 12/8/15.
//  Copyright © 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface gestgidTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelPlaceName;

@property (weak, nonatomic) IBOutlet UILabel *labelPlaceAddress;

@end
