//
//  LoadingInformationViewController.m
//  Pickalbatros
//
//  Created by User on 15.06.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "LoadingInformationViewController.h"
#import "Structure.h"

@interface LoadingInformationViewController (){
    float loadingProgress;
}

@property (nonatomic, strong) NSMutableData *downloadedData;

@property (nonatomic, strong) NSURL *jsonFileUrl;

@property (nonatomic,strong) NSArray *dataArray;

@property (nonatomic) int downloading;

@property (nonatomic) UIView *bgView;

@property (nonatomic) homeModel *homeModel;

@property (nonatomic) int rootID;

@property (nonatomic) int elementID;

@property (nonatomic) int numOfItems, currentItem;

@property (nonatomic) BOOL downloadFinished;

@end

@implementation LoadingInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _backgroundView.alpha = 0.0f;
    _popupView.alpha = 0.0f;
    _homeModel = [[homeModel alloc] init];
    loadingProgress = 0;
    [self setNeedsStatusBarAppearanceUpdate];
    if(!_skipUserInput){
        _headLineText = (_headLineText == nil)?_headLineText=NSLocalizedString(@"There is an update available for the selected place, do you want to download it now?",@""):_headLineText;
        
        _headLineLabel.text = _headLineText;
        [_okButton setTitle:(_okButtonText!=nil)?_okButtonText:NSLocalizedString(@"OK", @"") forState:UIControlStateNormal];
        [_cancelButton setTitle:(_cancelButtonText!=nil)?_cancelButtonText:NSLocalizedString(@"Cancel", @"") forState:UIControlStateNormal];
        [_continueButton setTitle:(_continueButtonText!=nil)?_continueButtonText:NSLocalizedString(@"Continue", @"") forState:UIControlStateNormal];
        
        _progressBar.hidden = YES;
    }
    
    
    if(_pID==0)
        _pID = (int)[globals sharedInstance].pID;
    
    if(_placeID==0)
        _placeID = (int)[globals sharedInstance].placeID;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    if(!_invisibleView){
        [UIView animateWithDuration:0.5 animations:^{_backgroundView.alpha = 0.5f;_popupView.alpha = 1.0f;}];
    }
    else {
        [UIView animateWithDuration:0.5 animations:^{
            _backgroundView.backgroundColor = [UIColor clearColor];_backgroundView.alpha = 1.0f;}];
    }
    if(_skipUserInput){
        [self okButtonPressed:nil];
    }
}



-(void)viewDidLayoutSubviews{
    if(!_invisibleView){
        self.popupView.layer.cornerRadius = 10.0f;
        self.popupView.backgroundColor = [UIColor clearColor];
        [self.popupView addBlurBehindSelfInView:self.view];
    }
}

-(void)downloadStructuresForPlace{
    
    _placeID = [[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceID"] intValue];
    // defining the URL
    NSString *urlString = [NSString stringWithFormat:@"%@get.php?pid=%i&getJsonStructuresForPlace=%i&languageID=%i", API_URL, _pID, _placeID,[languages currentLanguageID]];
    // Set the URL where we load from
    NSLog(@"url: %@",urlString);
    _jsonFileUrl = [NSURL URLWithString:urlString];
    _downloading = 1;
    
    if([self downloadBackgroundImageForPlace]){
        [self downloadItems];
    }
}

-(void)downloadPlaceInfo{
    
    // defining the URL
    NSString *urlString = [NSString stringWithFormat:@"%@get.php?pid=%i&getJsonPlace=%i&languageID=%i", API_URL, _pID, _placeID,[languages currentLanguageID]];
    // Set the URL where we load from
    
    _jsonFileUrl = [NSURL URLWithString:urlString];
    _downloading = 2;
    
    [self downloadItems];
}

- (void)downloadItems
{
    // Download the json file
    
    // Create the request
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:_jsonFileUrl];
    
    // Create the NSURLConnection
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}


-(void)showAsOverlay{
    
    CGSize mainScreenSize = [UIScreen mainScreen].bounds.size;
    _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainScreenSize.width, mainScreenSize.height)];
    _bgView.backgroundColor= [UIColor colorWithWhite:0.0f alpha:0.5f];
    _bgView.alpha = 0.0f;
    _popupView.alpha = 0.0f;
    
    [[UIApplication sharedApplication].keyWindow addSubview:_bgView];
    [[UIApplication sharedApplication].keyWindow addSubview:self.view];
    
    [UIView animateWithDuration:0.5 animations:^{
        _popupView.alpha = 1.0f;
        _bgView.alpha = 1.0f;
    }];
}


-(void)showAsOverlayOnViewController:(UIViewController*)vc{
    
    CGSize mainScreenSize = [UIScreen mainScreen].bounds.size;
    _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainScreenSize.width, mainScreenSize.height)];
    _bgView.backgroundColor= [UIColor colorWithWhite:0.0f alpha:0.5f];
    _bgView.alpha = 0.0f;
    _popupView.alpha = 0.0f;
    
    [vc.view addSubview:_bgView];
    [vc.view addSubview:self.view];
    
    [UIView animateWithDuration:0.5 animations:^{
        _popupView.alpha = 1.0f;
        _bgView.alpha = 1.0f;
    }];
}

- (IBAction)okButtonPressed:(id)sender {
    [_activityIndicator startAnimating];
    
    _okButton.hidden = YES;
    _cancelButton.hidden = YES;
    _okButton.userInteractionEnabled = NO;
    _cancelButton.userInteractionEnabled = NO;
    
    _progressBar.hidden = NO;
    
    _headLineLabel.text = NSLocalizedString(@"Loading contents", @"");
    
    [self downloadPlaceInfo];
}

- (IBAction)cancelButtonPressed:(id)sender {
    [UIView animateWithDuration:0.2 animations:^{self.backgroundView.alpha = 0.0f;self.view.alpha=0.0f;}
                     completion:^(BOOL finished){
                         [self dismissViewControllerAnimated:NO completion:nil];
                     }];
}

- (IBAction)continueButtonClicked:(id)sender {
    void (^onCompletion)(void);
    if(self.delegate){
        if([self.delegate respondsToSelector:@selector(hideLoadingInformationView)]){
            onCompletion = ^{
                [self.delegate hideLoadingInformationView];
            };
        }
    }
    [UIView animateWithDuration:0.2 animations:^{self.backgroundView.alpha = 0.0f;self.view.alpha=0.0f;}
                     completion:^(BOOL finished){
                         [self dismissViewControllerAnimated:NO completion:onCompletion];
                     }];
}


#pragma mark NSURLConnectionDataProtocol Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    // Parse the JSON that came in
    if(_downloading == 1){
        NSError *error;
        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingAllowFragments error:&error];
        [[localStorage storageWithFilename:@"structures"] setObject:jsonArray forKey:[NSString stringWithFormat:@"structureListForPlace%i",_placeID ]];
        _dataArray = jsonArray;
        
        int numOfItems = (int)[jsonArray count];
        int i = 0;
        for(NSDictionary *item in jsonArray){
            if([self loadImageForItem:item] != nil){
                
            }
            i++;
            if(i >= numOfItems){
                [self structureDownloadFinished];
            }
        }
    }
    if(_downloading == 2){
        NSError *error;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingAllowFragments error:&error];
        
        [[NSUserDefaults standardUserDefaults] setInteger:[jsonDict[@"uid"] integerValue] forKey:@"selectedPlaceID"];
        [[NSUserDefaults standardUserDefaults] setObject:jsonDict[@"name"] forKey:@"selectedPlaceName"];
        [[NSUserDefaults standardUserDefaults] setObject:jsonDict[@"placeBackgroundColor"] forKey:@"placeBackgroundColor"];
        [[NSUserDefaults standardUserDefaults] setObject:jsonDict[@"placeTextColor"] forKey:@"placeTextColor"];
        [[NSUserDefaults standardUserDefaults] setObject:jsonDict[@"tstamp"] forKey:@"placeVersionTimestamp"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        localStorage *placeStorage = [localStorage storageWithFilename:[NSString stringWithFormat:@"place%i",_placeID]];
        
        localStorage *currentPlaceStorage = [localStorage storageWithFilename:@"currentPlace"];
        
        
        // check if we already have some data
        NSDictionary *placeDict = [placeStorage objectForKey:@"selectedPlaceDictionary"];
        
        if(placeDict!=nil){
            // if our existing data has the same tstamp as the downloaded
            if([placeDict[@"tstamp"] intValue] == [jsonDict[@"tstamp"] intValue]){
                // update the file and stop the loading process
                [currentPlaceStorage setObject:jsonDict forKey:@"selectedPlaceDictionary"];
                
                // stop the loading process
                [self.delegate hideLoadingInformationView];
                
                return;
            }
        }
        
        [currentPlaceStorage setObject:jsonDict forKey:@"selectedPlaceDictionary"];
        [placeStorage setObject:jsonDict forKey:@"selectedPlaceDictionary"];
        
        NSDictionary *jsonElement = jsonDict;
        if(jsonElement[@"checkout_elements"] != nil && jsonElement[@"checkout_elements"] != NULL && ![jsonElement[@"checkout_elements"] isKindOfClass:[NSNull class]]){
            if([jsonElement[@"checkout_elements"] isKindOfClass:[NSArray class]] && [jsonElement[@"checkout_elements"] count] > 0){
                [[localStorage storageWithFilename:@"checkout"] setObject:jsonElement[@"checkout_elements"] forKey:[NSString stringWithFormat:@"checkoutElementsForPlace%i",[jsonElement[@"uid"] intValue]]];
                
            }
        }
        
        [self downloadStructuresForPlace];
        
        
    }
    
}

-(void)structureDownloadFinished{
    _activityIndicator.alpha = 0.0f;
    NSDictionary *structureDict = _dataArray[0];
    
    _numOfItems = (int)[_dataArray count];
    _currentItem = 0;
    _elementID = [structureDict[@"root_element_id"] intValue];
    _rootID = [structureDict[@"root_element_id"] intValue];
    [self rootItemsDownloaded:nil];
}

-(void)downloadStarted{
    _activityIndicator.alpha = 1.0f;
    _downloadFinished = NO;
}

-(void)rootItemsDownloaded:(NSMutableArray *)_items{
    if(_currentItem < _numOfItems){
        NSDictionary *structureDict = _dataArray[_currentItem];
        
        _elementID = [structureDict[@"root_element_id"] intValue];
        int rootID = [structureDict[@"root_element_id"] intValue];
        int elementID = [structureDict[@"root_element_id"] intValue];
        NSString *urlString = [NSString stringWithFormat:@"%@get.php?pid=%i&getJsonElementsFor=%i&getViewtypeForRootID=%i%@&languageID=%i", API_URL, _pID, elementID, rootID, @"&getMasterDescription", [languages currentLanguageID]];
        int selectedOfferCampaignID = 0;
        
        if(
           structureDict[@"offers"]!=nil &&
           ![structureDict[@"offers"] isKindOfClass:[NSNull class]] &&
           [structureDict[@"offers"] count] > 0 &&
           !([structureDict[@"hide_in_terminal_mode"] boolValue] && [globals sharedInstance].terminalMode)
           ){
            int offerCampaignID = [structureDict[@"campaign_id"] intValue];
            
            NSString *jsonStringMD5 = structureDict[@"offers_md5"];
            
            if(jsonStringMD5==nil)jsonStringMD5=@"";
            
            NSDictionary *offers = @{
                                     @"offerJsonStringMD5":jsonStringMD5,
                                     @"offers":structureDict[@"offers"]
                                     };
            
            [[localStorage storageWithFilename:@"offers"] setObject:offers forKey:[NSString stringWithFormat:@"offersForCampaign%i",offerCampaignID]];
            
            if(selectedOfferCampaignID==0){
                selectedOfferCampaignID=offerCampaignID;
            }
        }
        
        if(selectedOfferCampaignID!=0){
            [[globals sharedInstance] setOfferCampaignID:selectedOfferCampaignID];
        }
        
        homeModel *hm = _homeModel;
        // Set the element id (for saving the json locally)
        hm.elementID = elementID;
        // Set the URL where we load from
        hm.jsonFileUrl = [NSURL URLWithString:urlString];
        
        hm.isStillLoading = YES;
        // Set this view controller object as the delegate for the home model object
        hm.delegate = self;
        
        hm.rootItems = YES;
        
        _activityIndicator.alpha = 0.0f;
        // Call the download items method of the home model object
        [self downloadStarted];
        [hm performSelector:@selector(downloadItems) withObject:nil afterDelay:0.0f];
        
        _currentItem++;
    }
}

-(void)itemsDownloaded:(NSMutableArray *)items{

    for(Structure *item in items){
        if(item.childs > 0){
            
            [self downloadStarted];
            
            _elementID = item.uid;
            int elementID = item.uid;
            NSString *urlString = [NSString stringWithFormat:@"%@get.php?pid=%i&getJsonElementsFor=%i&getViewtypeForRootID=%i%@&languageID=%i", API_URL, _pID, elementID, _rootID, @"&getMasterDescription", [languages currentLanguageID]];
            
            homeModel *hm = [[homeModel alloc] init];
            
            // Set the element id (for saving the json locally)
            hm.elementID = elementID;
            
            // Set the URL where we load from
            hm.jsonFileUrl = [NSURL URLWithString:urlString];
            
            hm.isStillLoading = YES;
            
            // Set this view controller object as the delegate for the home model object
            hm.delegate = self;
            
            // Call the download items method of the home model object
            [self downloadStarted];
            [hm performSelector:@selector(downloadItems) withObject:nil afterDelay:0.0f];
        }
    }
    
    _downloadFinished = YES;
    [self performSelector:@selector(downloadCompletelyFinished) withObject:nil afterDelay:5.0f];
    if(loadingProgress < 0.5f){
        loadingProgress+=0.05;
    } else if(loadingProgress < 0.75f) {
        loadingProgress+=0.02;
    } else if(loadingProgress < 0.9f){
        loadingProgress+=0.01;
    }
    self.progressBar.progress = loadingProgress;
    
}

-(void) downloadCompletelyFinished{
    _activityIndicator.alpha = 0.0f;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"itemsDownloaded"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sidebarUpdate" object:nil];
    if(self.downloadFinished){
        if(!_skipUserInput){
            self.progressBar.progress = 1;
            self.continueButton.hidden = NO;
            self.progressBar.hidden = YES;
            self.headLineLabel.text = NSLocalizedString(@"Download finished", @"");
        } else {
            [self continueButtonClicked:nil];
        }
    }
}

-(void)downloadSubItemsForItems:(NSArray*)items{
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{

    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",@"") message:error.localizedDescription delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil, nil] show];
}

-(BOOL)downloadBackgroundImageForPlace{
    imageMethods *images = [[imageMethods alloc] init];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSDictionary *selectedPlaceDictionary = [[localStorage storageWithFilename:@"currentPlace"] objectForKey:@"selectedPlaceDictionary"];
    
    NSString *sliderImagesString = selectedPlaceDictionary[@"app_images_slider"];
    
    NSArray *imagePaths = [sliderImagesString componentsSeparatedByString:@","];
    
    NSString *structureImageSm = imagePaths[0];
    
    NSString *imageNameFromTstamp = [NSString stringWithFormat:@"%@-%@",[[structureImageSm lastPathComponent]stringByDeletingPathExtension],selectedPlaceDictionary[@"uid"]];
    
    if(structureImageSm != nil && ![structureImageSm isKindOfClass:[NSNull class]]){
        
        if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",imageNameFromTstamp]]]){
            // if png image exists:
            //Load Image From Directory
            [images loadImage:imageNameFromTstamp ofType:@"png" inDirectory:documentsPath];
        }
        else if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageNameFromTstamp]]]){
            // if jpg image exists:
            //Load Image From Directory
            [images loadImage:imageNameFromTstamp ofType:@"jpg" inDirectory:documentsPath];
        }
        else {
            //Get Image From URL
            UIImage * imageFromURL = [images getImageFromURL:[NSString stringWithFormat:@"%@/%@?w=%f",SYSTEM_DOMAIN,structureImageSm, [UIScreen mainScreen].bounds.size.width*3.0f]];
            
            NSLog(@"url: %@", [NSString stringWithFormat:@"%@/%@?w=%f",SYSTEM_DOMAIN,structureImageSm, [UIScreen mainScreen].bounds.size.width*3.0f]);
            
            NSString *imageType = @"";
            if([structureImageSm hasSuffix:@".png"] ||
               [structureImageSm hasSuffix:@".PNG"]){
                imageType = @"png";
            }
            else if([structureImageSm hasSuffix:@".jpg"] ||
                    [structureImageSm hasSuffix:@".jpeg"] ||
                    [structureImageSm hasSuffix:@".JPEG"] ||
                    [structureImageSm hasSuffix:@".JPG"]){
                imageType = @"jpg";
            }
            
            //Save Image to Directory
            [images saveImage:imageFromURL withFileName:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
            
            //Load Image From Directory
            [images loadImage:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
        }
        
    }
    
    return YES;
}

-(UIImage*)loadImageForItem:(NSDictionary*)item{
    
    if(self.progressBar.progress<0.3f){
        self.progressBar.progress+=0.02;
        loadingProgress+=0.02;
    }
    
    imageMethods *images = [[imageMethods alloc] init];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *imageNameFromTstamp = [NSString stringWithFormat:@"%@-%@",item[@"tstamp"],item[@"uid"]];
    UIImage *cellImage = nil;
    NSString *structureImageSm = item[@"image_sm"];
    if(structureImageSm != nil && ![structureImageSm isKindOfClass:[NSNull class]]){
        
        if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",imageNameFromTstamp]]]){
            // if png image exists:
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:@"png" inDirectory:documentsPath];
            return cellImage;
        }
        else if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageNameFromTstamp]]]){
            // if jpg image exists:
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:@"jpg" inDirectory:documentsPath];
            return cellImage;
        }
        else {
            //Get Image From URL
            
            // 40px icons
            float imageDevicePoints = 40;
            float mainScreenHeight = [UIScreen mainScreen].bounds.size.height;
            if(mainScreenHeight>=568){
                imageDevicePoints = 40; //iPhone 5
            }
            if(mainScreenHeight>=667){
                imageDevicePoints = 63; //iPhone 6
            }
            if(mainScreenHeight>=736){
                imageDevicePoints = 80; // iPhone 6+
            }
            
            UIImage * imageFromURL = [images getImageFromURL:[NSString stringWithFormat:@"%@/%@?w=%f",SYSTEM_DOMAIN,structureImageSm,imageDevicePoints*2]];
            NSLog(@"url: %@", [NSString stringWithFormat:@"%@/%@?w=%f",SYSTEM_DOMAIN,structureImageSm,imageDevicePoints*2]);
            
            NSString *imageType = @"";
            if([structureImageSm hasSuffix:@".png"] ||
               [structureImageSm hasSuffix:@".PNG"]){
                imageType = @"png";
            }
            else if([structureImageSm hasSuffix:@".jpg"] ||
                    [structureImageSm hasSuffix:@".jpeg"] ||
                    [structureImageSm hasSuffix:@".JPEG"] ||
                    [structureImageSm hasSuffix:@".JPG"]){
                imageType = @"jpg";
            }
            
            //Save Image to Directory
            [images saveImage:imageFromURL withFileName:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
            
            //Load Image From Directory
            cellImage = [images loadImage:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
            
            return cellImage;
        }
        
    }
    
    return cellImage;
}

-(void)homeModelDownloadFailedWithError:(NSString *)error{
    [self cancelButtonPressed:nil];
}

@end
