//
//  AppDelegate.m
//  pickalbatros
//
//  Created by User on 09.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "AppDelegate.h"
#import "PlaceOverViewController.h"
#import "StructureOverViewController.h"
#import "NavigationViewController.h"
#import "SWRevealViewController.h"
#import "OnboardingViewController.h"
#import "notificationListViewController.h"
#import "WebViewController.h"
#import "OfferViewController.h"

#import "gestgidStartViewController.h"

#import "AppVersionManagement.h"

#import "DBManager.h"

#import "StartTabBarViewController.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface AppDelegate ()

@property (nonatomic, strong) DBManager *dbManager;

@property (nonatomic, strong) NSString *dbNotificationTitle, *dbNotificationDescription, *dbNotificationUrl;
@property (nonatomic) int dbNotificationTstamp;

@property (nonatomic, strong) AppVersionManagement *appVersionManagement;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    self.dbManager = [[DBManager alloc]initWithDatabaseFilename:@"db_template.sql"];
    [globals sharedInstance];
    
    
    NSUserDefaults *standardDefaults = [NSUserDefaults standardUserDefaults];
    
    
    // checking current and previous builds
    int settingsBuildVersion = [[standardDefaults objectForKey:@"settingsBundleVersion"] intValue];
    int buildVersion = [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"] intValue];
    
    NSLog(@"settingsBuildVersion: %i",settingsBuildVersion);
    NSLog(@"buildVersion: %i",buildVersion);
    
    // if these are not matching
    if(settingsBuildVersion != buildVersion){
        // delete all cached information (except images, these shouldn't change)
        
        
        // if the user is logged in
        if([standardDefaults integerForKey:@"userLoggedIn"] == 1){
            // log out the user and remove the settings and everything, is usefull if we have some changes in the settings or anything
            [standardDefaults removeObjectForKey:@"loggedInUserID"];
            [standardDefaults removeObjectForKey:@"userLoggedIn"];
            //if(SINGLEPLACEID==0)[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"selectedPlaceID"];
            
            [[localStorage storageWithFilename:@"settings"] clearFile];
            [[localStorage storageWithFilename:@"dynamicUserSettingsValues"] clearFile];
        }
        
        // set the new buildID as the settings bundle
        [standardDefaults setObject:[NSNumber numberWithInt:buildVersion] forKey:@"settingsBundleVersion"];
        [standardDefaults synchronize];
    }
    
    
    // other setup tasks here....
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
#ifdef __IPHONE_8_0
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert
                                                                                             |UIUserNotificationTypeBadge
                                                                                             |UIUserNotificationTypeSound) categories:nil];
        [application registerUserNotificationSettings:settings];
#endif
    }
    
    _appVersionManagement = [[AppVersionManagement alloc] init];
    [_appVersionManagement performSelector:@selector(checkForUpdates) withObject:nil afterDelay:5.0f];
    
    
    if([standardDefaults integerForKey:@"userLoggedIn"] == 1 || ([standardDefaults integerForKey:@"selectedPlaceID"] != 0 && [globals sharedInstance].terminalMode)){
        int userID = [[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedInUserID"] intValue];
        [deviceTokenHandling updateDeviceToken:@"" forUserID:userID];
        
        if([globals sharedInstance].placeID != 0){
            self.sbvc = [SidebarViewController loadNowFromStoryboard:@"Sidebar"];
            
            StartTabBarViewController *tabController = [StartTabBarViewController new];
            
            SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:self.sbvc frontViewController:tabController];
            
            self.window.rootViewController = rvc;
            //[ovc setSideBarGestureFor:rvc];
            
        } else {
            self.sbvc = [SidebarViewController loadNowFromStoryboard:@"Sidebar"];
            self.sbvc.view.tag = 1;
            
            UIViewController *vc = [AppDelegate getStartViewController];
            
            NavigationViewController *nvc = [[NavigationViewController alloc] initWithRootViewController:vc];
            
            SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:self.sbvc frontViewController:nvc];
            
            self.window.rootViewController = rvc;
        }
        
    } else {
        OnboardingViewController *ovc = (OnboardingViewController*)[[UIStoryboard storyboardWithName:@"LoginAndRegister" bundle:nil] instantiateInitialViewController];
        
        SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:self.sbvc frontViewController:ovc];
        self.window.rootViewController = rvc;
    }
    //int selectedPlaceID = [[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceID"] intValue];
    //if(selectedPlaceID != 0){[[ifbckAPIConnector instantiate] downloadAllImagesForPlace:selectedPlaceID];}
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    
    NSDictionary *notifications = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];
    if (notifications) {
        NSLog(@"Push notification triggered launch: %@", notifications);
        [self application:application didReceiveRemoteNotification:notifications];
    }
    
    return YES;
}


- (void)applicationDidFinishLaunching:(UIApplication *)app {
}

// Delegation methods
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
    
    self.deviceToken = devToken;
    
    NSString *token = [NSString stringWithFormat:@"%@", devToken];
    
    //format token
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    NSLog(@"token registered, save token to server: %@", token);
    [deviceTokenHandling registerDeviceToken:token];
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSLog(@"Error in registration. Error: %@", err);
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    
    NSLog(@"userInfo: %@", userInfo);
    
    NSString *titleToShow = @"", *textToShow= @"", *theUrl = @"", *buttonTitle = @"";
    int tstamp;
    
    //predefining the text which will be shown in the alert
    if([[userInfo valueForKey:@"aps"] valueForKey:@"alert"]){
        textToShow = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
    } else {
        textToShow = @"Empty notification?";
    }
    
    //predefining the title which will be shown in the alert
    if([userInfo valueForKey:@"t"]){
        titleToShow = [userInfo valueForKey:@"t"];
    } else {
        titleToShow = @"Benachrichtigung";
    }
    
    //predefining the title, which will be used on the webview (if there is a url given)
    if([userInfo valueForKey:@"title"]){
        self.titleForWebsite = [userInfo valueForKey:@"title"];
    } else {
        self.titleForWebsite = @"iFeedback®";
    }
    
    //predefining the second button (index 1) if url is given in the notification
    if([userInfo valueForKey:@"url"]){
        
        theUrl = [userInfo valueForKey:@"url"];
        
        // add http if it has been forgotten
        if(![theUrl hasPrefix:@"http://"]){
            if(![theUrl hasPrefix:@"https://"]){
                theUrl = [NSString stringWithFormat:@"http://%@", theUrl];
            }
        }
        
        //textToShow = [textToShow stringByAppendingString:@"\n"];
        //textToShow = [textToShow stringByAppendingString:[userInfo valueForKey:@"url"]];
        self.urlToOpen = theUrl;
        buttonTitle = @"OK";
    } else {
        self.urlToOpen = @"";
        //        buttonTitle = nil;
        buttonTitle = @"OK";
    }
    
    if([userInfo valueForKey:@"tstamp"]){
        tstamp = [[userInfo valueForKey:@"tstamp"] intValue];
    } else {
        tstamp = 0;
    }
    
    
    if([[UIApplication sharedApplication]  applicationState] == UIApplicationStateBackground){
        
    } else if ([[UIApplication sharedApplication]  applicationState] == UIApplicationStateInactive){
        
    }
    
    if([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive){
        
        SWRevealViewController *rvc = (SWRevealViewController*)self.window.rootViewController;
        NavigationViewController *nvc = (NavigationViewController*)rvc.frontViewController;
        
        // if we aren't in the notification view
        if(![nvc.viewControllers[0] isKindOfClass:[notificationListViewController class]]){
            
            //create the notification alert
            self.notificationAlert = [[UIAlertView alloc]
                                      initWithTitle:titleToShow message:[NSString stringWithFormat:@"%@", textToShow] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: buttonTitle, nil];
            
            //and show it
            [self.notificationAlert show];
        }
        
    } else {
        
        
        //new instance of webViewController
        UIViewController *vc = [UIViewController new];
        NSLog(@"theUrl: %@", theUrl);
        if(![theUrl isEmptyString]){
            NSLog(@"going to web view");
            WebViewController *wv = [WebViewController loadNowFromStoryboard:@"WebView"];
            
            wv.titleString = titleToShow;
            wv.url = [NSURL URLWithString:theUrl];
            wv.urlGiven = YES;
            vc = wv;
        }
        else {
            NSLog(@"going to notification list");
            notificationListViewController *nlvc = [notificationListViewController loadNowFromStoryboard:@"Notification"];
            nlvc.showSidebarButton = YES;
            vc = nlvc;
        }
        //go to instance
        
        NavigationViewController* nc = [[NavigationViewController alloc] initWithRootViewController:vc];
        
        SWRevealViewController *rvc = [[SWRevealViewController alloc] initWithRearViewController:[SidebarViewController loadNowFromStoryboard:@"Sidebar"] frontViewController:nc];
        
        vc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"]
                                                                               style:UIBarButtonItemStylePlain
                                                                              target:rvc
                                                                              action:@selector(revealToggle:)];
        
        self.window.rootViewController = rvc;
    }
    
    
    //set Application Badge even if app is in the foreground
    //NSString* alertValue = [[userInfo valueForKey:@"aps"] valueForKey:@"badge"];
    
    self.dbNotificationTitle = titleToShow;
    self.dbNotificationDescription = textToShow;
    self.dbNotificationUrl = theUrl;
    self.dbNotificationTstamp = tstamp;
    
    // Form the query.
    NSString *query = [NSString stringWithFormat:@"select * from notifications WHERE notificationTstamp = %i", tstamp];
    
    NSArray *tempArray = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    int amount = (int) tempArray.count;
    
    NSLog(@"notifications with tstamp: %@", tempArray);
    
    NSLog(@"amount of notifications with tstamp: %i", amount);
    
    if(amount == 0){
        NSLog(@"saving notification to database");
        
        [self saveNotificationDataToDatabse];
        
        [self updateAppBadgeFromDatabase];
    }
}


-(void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(alertView == self.notificationAlert){
        if(buttonIndex != 0){
            //UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SWRevealViewController *navigationController = (SWRevealViewController *)self.window.rootViewController;
            UIViewController *dvc = nil;
            if(self.urlToOpen != nil && ![self.urlToOpen isEqualToString:@""]){
                //new instance of webViewController
                WebViewController *wv = [WebViewController loadNowFromStoryboard:@"WebView"];
                
                wv.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"]
                                                                                       style:UIBarButtonItemStylePlain
                                                                                      target:wv.revealViewController
                                                                                      action:@selector(revealToggle:)];
                
                wv.titleString = self.titleForWebsite;
                wv.url = [NSURL URLWithString:self.urlToOpen];
                wv.urlGiven = YES;
                dvc = wv;
            } else {
                //new instance of webViewController
                notificationListViewController *wv = [notificationListViewController loadNowFromStoryboard:@"Notification"];
                wv.showSidebarButton = YES;
                dvc = wv;
            }
            
            //[wv prepareWebsite];
            
            //go to instance
            
            NavigationViewController *nc = [[NavigationViewController alloc] initWithRootViewController:dvc];
            
            [navigationController pushFrontViewController:nc animated:YES];
        }
    }
    
}

- (void)application:(UIApplication *)application
handleActionWithIdentifier:(NSString *)identifier
forRemoteNotification:(NSDictionary *)userInfo
  completionHandler:(void(^)())completionHandler{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
    
    [self application:application didReceiveRemoteNotification:userInfo];
    NSString *titleToShow, *textToShow, *theUrl;
    int tstamp;
    
    //predefining the text which will be shown in the alert
    if([[userInfo valueForKey:@"aps"] valueForKey:@"alert"]){
        textToShow = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
    } else {
        textToShow = @"Empty notification?";
    }
    
    //predefining the title which will be shown in the alert
    if([userInfo valueForKey:@"t"]){
        titleToShow = [userInfo valueForKey:@"t"];
    } else {
        titleToShow = @"Benachrichtigung";
    }
    
    //predefining the title, which will be used on the webview (if there is a url given)
    if([userInfo valueForKey:@"title"]){
        self.titleForWebsite = [userInfo valueForKey:@"title"];
    } else {
        self.titleForWebsite = @"iFeedback®";
    }
    
    //predefining the second button (index 1) if url is given in the notification
    if([userInfo valueForKey:@"url"]){
        
        theUrl = [userInfo valueForKey:@"url"];
        
        // add http if it has been forgotten
        if(![theUrl hasPrefix:@"http://"]){
            if(![theUrl hasPrefix:@"https://"]){
                theUrl = [NSString stringWithFormat:@"http://%@", theUrl];
            }
        }
        
        self.urlToOpen = theUrl;
    } else {
        self.urlToOpen = @"";
    }
    
    if([userInfo valueForKey:@"tstamp"]){
        tstamp = [[userInfo valueForKey:@"tstamp"] intValue];
    } else {
        tstamp = 0;
    }
    
    self.dbNotificationTitle = titleToShow;
    self.dbNotificationDescription = textToShow;
    self.dbNotificationUrl = theUrl;
    self.dbNotificationTstamp = tstamp;
    [self saveNotificationDataToDatabse];
    [self updateAppBadgeFromDatabase];
}

- (void)applicationWillResignActive:(UIApplication *)application{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    //[self.locationManager stopUpdatingLocation];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"appDidBecomeInactive" object:nil];
    [self updateAppBadgeFromDatabase];
}

- (void)applicationWillEnterForeground:(UIApplication *)application{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    //reset the badge number after app did reappeared
    //[[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"appDidBecomeActive" object:nil];
    
    
    _appVersionManagement = [[AppVersionManagement alloc] init];
    //[_appVersionManagement checkForUpdates];
    [_appVersionManagement performSelector:@selector(checkForUpdates) withObject:nil afterDelay:2.0f];
    
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application{
    [FBSDKAppEvents activateApp];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



- (void)saveNotificationDataToDatabse{
    // Prepare the query string.
    NSString *query = [NSString stringWithFormat:@"insert into notifications (notificationTitle, notificationDescription, notificationURL, notificationTstamp, has_been_read) values('%@', '%@', '%@', %i, 0)", self.dbNotificationTitle, self.dbNotificationDescription, self.dbNotificationUrl, self.dbNotificationTstamp];
    
    NSLog(@"Querry to run: %@", query);
    
    // Execute the query.
    [self.dbManager executeQuery:query];
    
    // If the query was successfully executed then pop the view controller.
    if (self.dbManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CAME_IN object:@""];
    }
    else{
        NSLog(@"Could not execute the query.");
    }
}

- (void)updateAppBadgeFromDatabase{
    // get the amount of unread messages and display it as a badge
    // Form the query.
    NSString *query = @"select * from notifications WHERE has_been_read = 0";
    
    NSArray *tempArray = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    int amount = (int) tempArray.count;
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:amount];
}


#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.bhmms.pickalbatros" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"pickalbatros" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"pickalbatros.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

+(UIViewController*)getStartViewController{
    if(SINGLEPLACEID==0){
        gestgidStartViewController *vc = [gestgidStartViewController loadNowFromStoryboard:@"gestgidStart"];
        if(IDIOM==IPAD)vc = [gestgidStartViewController loadNowFromStoryboard:@"gestgidStart-iPad"];
        
        return vc;
    }
    else {
        StartTabBarViewController *stvc = [StartTabBarViewController new];
        return stvc;
    }
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    NSLog(@"application LISDKCallbackHandler url,%@\n sourceApplication, %@ ", url, sourceApplication);
    
    if ([LISDKCallbackHandler shouldHandleUrl:url]) {
        return [LISDKCallbackHandler application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    } else {
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation];
    }
    return YES;
}

@end