//
//  CheckInTableViewCell.h
//  KAMEHA
//
//  Created by User on 23.02.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckInTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIView *dateFromView;
@property (weak, nonatomic) IBOutlet UILabel *labelFromDay;
@property (weak, nonatomic) IBOutlet UILabel *labelFromMonth;
@property (weak, nonatomic) IBOutlet UILabel *labelFromTime;


@property (weak, nonatomic) IBOutlet UIView *dateToView;
@property (weak, nonatomic) IBOutlet UILabel *labelToDay;
@property (weak, nonatomic) IBOutlet UILabel *labelToMonth;
@property (weak, nonatomic) IBOutlet UILabel *labelToTime;



@property (weak, nonatomic) IBOutlet UITextView *textViewWishes;

@property (strong, nonatomic) IBOutlet UIButton *checkInButton;

@property (strong, nonatomic) IBOutlet UIButton *registerButton;

@end
