#import <UIKit/UIKit.h>

@interface UIView (MWLayoutHelpers)

@property (assign, nonatomic) CGFloat x;
@property (assign, nonatomic) CGFloat y;

@property (assign, nonatomic) CGFloat top;
@property (assign, nonatomic) CGFloat bottom;
@property (assign, nonatomic) CGFloat left;
@property (assign, nonatomic) CGFloat right;

@property (assign, nonatomic) CGFloat width;
@property (assign, nonatomic) CGFloat height;

@property (assign, nonatomic) CGSize size;
@property (assign, nonatomic) CGPoint origin;

@property (NS_NONATOMIC_IOSONLY, readonly) CGFloat bottomOfSuperview;
@property (NS_NONATOMIC_IOSONLY, readonly) CGFloat rightOfSuperview;

- (void)moveToPoint:(CGPoint)point;
- (void)moveBy:(CGPoint)pointDelta;
- (void)resizeTo:(CGSize)size;

- (void)setBackgroundBlur;
- (void)addBlurBehindSelfInView:(UIView*)view;
-(void)addViewWithColor:(UIColor*)color alpha:(float)alpha behindSelfInView:(UIView*)view;

-(UIColor*)getColorFromHexString:(NSString*)colorString;

@end