//
//  NSObject+IsEmptyString.m
//  if_framework
//
//  Created by User on 3/14/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "NSObject+IsEmptyString.h"

@implementation NSObject (IsEmptyString)

-(BOOL)isEmptyString{
    if(
       self!=nil &&
       [self isKindOfClass:[NSString class]] &&
       ![[((NSString*)self) stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] &&
       ![[((NSString*)self) stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@"(null)"]
       ){
        return NO;
    }
    
    return YES;
}

@end
