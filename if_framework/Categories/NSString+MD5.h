#import "NSString+MD5.h"

@interface NSString (MD5)

@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *MD5String;

@end
