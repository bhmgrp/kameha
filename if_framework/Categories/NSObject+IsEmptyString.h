//
//  NSObject+IsEmptyString.h
//  if_framework
//
//  Created by User on 3/14/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (IsEmptyString)

@property (NS_NONATOMIC_IOSONLY, getter=isEmptyString, readonly) BOOL emptyString;

@end
