//
//  Message.h
//  if_framework
//
//  Created by Julian Böhnke on 17.02.16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject

+ (void) alert:(NSString *)msg Title:(NSString *)title Tag:(int)tag;

@end
