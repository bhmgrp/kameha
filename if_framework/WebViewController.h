//
//  WebViewController.h
//  pickalbatros
//
//  Created by User on 13.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController <UIWebViewDelegate>


@property (weak, nonatomic) IBOutlet UIWebView *webView;


@property (strong, nonatomic) NSString *titleString;
@property (strong, nonatomic) NSURL *url;
@property (nonatomic) BOOL urlGiven, hideSharebutton;
@property (nonatomic) BOOL isLoading;
@property (nonatomic) BOOL hideSideBarButton;
@property (strong, nonatomic) NSString *currentURL;

@property (nonatomic) NSString *htmlString;
@property (nonatomic) BOOL useHTML;

-(void)hideModalWebView;

+(WebViewController*)getWebViewController;

@end
