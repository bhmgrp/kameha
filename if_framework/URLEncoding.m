//
//  URLEncoding.m
//  if_framework
//
//  Created by Julian Böhnke on 15.02.16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//
#import "NSString+URLEncoding.h"

@implementation NSString (encode)
- (NSString *)encodeString:(NSStringEncoding)encoding
{
    return (NSString *) CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)self,
                                                                NULL, (CFStringRef)@";/?:@&=$+{}<>,",
                                                                CFStringConvertNSStringEncodingToEncoding(encoding));
}  
@end