//
//  NavigationTitle.m
//  if_framework
//
//  Created by Julian Böhnke on 31.03.16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "NavigationTitle.h"

@implementation NavigationTitle


+ (UIView *) createNavTitle:(NSString *)title SubTitle:(NSString *)subTitle{
    
    CGFloat xPosition = 0;
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].applicationFrame.size.width - 130, 64)];
    
    if (title != nil && ![title isEqualToString:@""]) {
        
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(xPosition, -10, titleView.frame.size.width, titleView.frame.size.height)];
        titleLabel.textColor = [UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        
        [titleLabel setText: title];
        [titleView addSubview:titleLabel];
    }
    
    if (subTitle != nil && ![subTitle isEqualToString:@""]) {
        
        UILabel *subTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(xPosition, 10, titleView.frame.size.width, titleView.frame.size.height)];
        subTitleLabel.textColor = [UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR];
        subTitleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:11.0];
        subTitleLabel.textAlignment = NSTextAlignmentCenter;
        
        [subTitleLabel setText: subTitle];
        
        [titleView addSubview:subTitleLabel];
    }
    
    return titleView;
}


@end
