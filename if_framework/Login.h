//
//  Login&Signup.h
//  if_framework
//
//  Created by Julian Böhnke on 18.02.16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Unirest/UNIRest.h"

@interface Login : NSObject

@property NSDictionary* postData;
@property NSString* url;

@property bool overSocial;

- (UNIHTTPJsonResponse*) callAPI;

- (void) markUserAsLoggedIn:(NSDictionary*) userData;

@end
