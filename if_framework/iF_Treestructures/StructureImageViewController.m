//
//  StructureImageViewController.m
//  if_framework
//
//  Created by User on 1/22/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "StructureImageViewController.h"
#import "imageMethods.h"

#import "MBProgressHUD.h"

@interface StructureImageViewController (){
    imageMethods *images;
    NSString *documentsPath;
}

@end

@implementation StructureImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.scrollView.minimumZoomScale=1.0;
    self.scrollView.maximumZoomScale=6.0;
    
    self.scrollView.delegate = self;
    
    //self.imageView.image = self.image;
    
    
    images = [[imageMethods alloc] init];
    documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    
    // Loading Screen
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedString(@"Loading",@"");
    
    self.imageView.image = [self createImageFromWebPath:self.imageServerPath];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(instancetype)initWithImage:(UIImage*)image{
    self = [self init];
    
    self.imageView.image = image;
    
    return self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return self.imageView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^(void){}];
}


-(UIImage*)createImageFromWebPath:(NSString*)path{
    UIImage *cellImage = nil;
    
    NSString *structureImageSm = [path stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    NSString *imageNameFromTstamp = [NSString stringWithFormat:@"%@-imageDetails",(![structureImageSm isKindOfClass:[NSNull class]])?[[structureImageSm lastPathComponent]stringByDeletingPathExtension]:@""];
    NSLog(@"image name: %@", imageNameFromTstamp);
    // if we have a png with that filename load it
    if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",imageNameFromTstamp]]]){
        cellImage = [images loadImage:imageNameFromTstamp ofType:@"png" inDirectory:documentsPath];
        NSLog(@"png image reused: %@", imageNameFromTstamp);
    }
    // else if we have a jpg with that filename load it
    else if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageNameFromTstamp]]]){
        cellImage = [images loadImage:imageNameFromTstamp ofType:@"jpg" inDirectory:documentsPath];
        NSLog(@"jpg image reused: %@", imageNameFromTstamp);
    }
    // else get it from the url
    else {
        UIImage * imageFromURL = [images getImageFromURL:[NSString stringWithFormat:@"%@/%@&w=%f",SYSTEM_DOMAIN,structureImageSm,[UIScreen mainScreen].bounds.size.width*3*3]];
        NSString *imageType = @"";
        if([structureImageSm hasSuffix:@".png"] ||
           [structureImageSm hasSuffix:@".PNG"]){
            imageType = @"png";
        }
        else if([structureImageSm hasSuffix:@".jpg"] ||
                [structureImageSm hasSuffix:@".jpeg"] ||
                [structureImageSm hasSuffix:@".JPEG"] ||
                [structureImageSm hasSuffix:@".JPG"]){
            imageType = @"jpg";
        }
        //NSLog(@"image from url: http://www.ifbck.com/%@",structureImageSm);
        //NSLog(@"documents path: %@",documentsPath);
        //NSLog(@"imageFromURL: %@",imageFromURL);
        //NSLog(@"imageNameFromTstamp: %@",imageNameFromTstamp);
        //NSLog(@"imageType: %@", imageType);
        //Save Image to Directory
        [images saveImage:imageFromURL withFileName:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
        
        //Load Image From Directory
        cellImage = [images loadImage:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
        NSLog(@"document path: %@", documentsPath);
        NSLog(@"new image saved: %@", imageNameFromTstamp);
    }
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    
    //cell.imageViewPlaceNavigation.image = cellImage;
    return cellImage;
}

@end
