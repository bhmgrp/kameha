//
//  homeModel.h
//  Database
//
//  Created by User on 16.10.14.
//  Copyright (c) 2014 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol homeModelProtocol <NSObject>

- (void)itemsDownloaded:(NSArray *)items;
- (void)rootItemsDownloaded:(NSArray *)items;
- (void)downloadStarted;
- (void)homeModelDownloadFailedWithError:(NSString*)error;

@end

@interface homeModel : NSObject <NSURLConnectionDataDelegate>

@property (nonatomic) int elementID;

@property (nonatomic, strong) NSMutableData *downloadedData;

@property (nonatomic, strong) id<homeModelProtocol> delegate;

@property (nonatomic, strong) NSURL *jsonFileUrl;

@property (nonatomic) BOOL rootItems, isStillLoading;

-(void)parseDownloadedData:(NSData*)downloadedData;


- (void)downloadItems;

@end
