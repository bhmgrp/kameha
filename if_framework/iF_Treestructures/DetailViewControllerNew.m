//
//  DetailViewControllerNew.m
//  pickalbatros
//
//  Created by User on 24.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "DetailViewControllerNew.h"
#import "DetailTableViewCell.h"
#import "imageMethods.h"
#import "MBProgressHud.h"

#import "StructureImageViewController.h"

@interface DetailViewControllerNew (){
    imageMethods *images;
    NSString *documentsPath;
    NSAttributedString *attrString;
}
@property (nonatomic) UIImage *cellImage;
@end

@implementation DetailViewControllerNew

- (void)viewDidLoad {
    [super viewDidLoad];
    
    images = [[imageMethods alloc] init];
    documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    
    self.webViewWrapper.width = [UIScreen mainScreen].bounds.size.width;
    self.descriptionWebView.delegate = self;
    self.descriptionWebView.scrollView.scrollEnabled = NO;
    self.descriptionWebView.scrollView.bounces = NO;
    [self.descriptionWebView setWidth:[UIScreen mainScreen].bounds.size.width - 4 - 4];
    _descriptionText = [NSString stringWithFormat:@"<span style=\"font-family:%@;font-size:%fpt;color:%@;\">%@</span>",
                        @"Gotham-Book",11.0,@"black",
                        _descriptionText];
    [self.descriptionWebView loadHTMLString:self.descriptionText baseURL:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    webView.height = webView.scrollView.contentSize.height;
    self.webViewWrapper.height = webView.scrollView.contentSize.height+4;
    
    // Do any additional setup after loading the view.
    if(self.preLoadImage){
        self.cellImage = [self createImageForElement];
        [self.tableView reloadData];
        return;
    }
    else {
        [self performSelectorInBackground:@selector(loadImage) withObject:nil];
    }
    
    [self.tableView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.viewType == 0){
        return (self.priceText == nil || [self.priceText isKindOfClass:[NSNull class]] || [((NSString*)self.priceText) isEqualToString:@""])?2:3; //prev: 4
    }
    return 2; // prev: 3
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    DetailTableViewCell *cell = [[DetailTableViewCell alloc] init];
    
    
    //image cell
    if(indexPath.row == 0){
        DetailTableViewCell *c = [tableView dequeueReusableCellWithIdentifier:@"imageCell"];
        
        c.imageCellImageView.image = self.cellImage;
        cell = c;
    }
    //header cell
    if(indexPath.row == 999){
        DetailTableViewCell *c = [tableView dequeueReusableCellWithIdentifier:@"headerInfoCell"];
        c.labelHeadline.text = self.headerText1;
        c.labelSubHeadline.text = self.headerText2;
        cell = c;
    }
    // description cell
    if(indexPath.row == 1){
        DetailTableViewCell *c = (DetailTableViewCell*)[self descriptionTableViewCell];
        c.textViewDescription.editable = NO;
        DetailTableViewCell *descriptionCell = [tableView dequeueReusableCellWithIdentifier:@"elementDescriptionCell2"];
        
        [descriptionCell addSubview:self.webViewWrapper];
        return descriptionCell;
    }
    // price cell
    if(indexPath.row == 2){
        DetailTableViewCell *c = [tableView dequeueReusableCellWithIdentifier:@"priceCell"];
        c.labelHeadline.text = NSLocalizedString(@"Price", @"");
        c.labelSubHeadline.text = self.priceText;
        cell = c;
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //image cell
    if(indexPath.row == 0){
        if(self.imageString != nil && self.cellImage != nil){
            return 209 * ([UIScreen mainScreen].bounds.size.width / 320);
        }
    }
    //header cell
    if(indexPath.row == 999){
        return 62;
    }
    // description cell
    if(indexPath.row == 1){
        
        return self.descriptionWebView.height+12;
    }
    // price cell
    if(indexPath.row == 2){
        if(self.viewType == 0 && self.priceText != nil && ![[((NSString*)self.priceText) stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
            return 62;
        }
    }
    
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section == 0 && indexPath.row == 0){
        StructureImageViewController *sivc = [StructureImageViewController loadNowFromStoryboard:@"StructureImageViewController"];
        
        sivc.imageServerPath = self.imageString;
        
        [self.navigationController presentViewController:sivc animated:YES completion:^(void){}];
    }
}

-(UIImage*)createImageForElement{
    
    UIImage *cellImage = nil;
    
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\!\\.\\:\\/\"\%\\&\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    NSString *newString = [regex stringByReplacingMatchesInString:self.headerText1 options:0 range:NSMakeRange(0, [self.headerText1 length]) withTemplate:@""];
    
    NSString *imageNameFromTstamp = [NSString stringWithFormat:@"%i-%i-%@",self.tstamp,self.uid,newString];
    NSString *imageName = [NSString stringWithFormat:@"%@",self.imageString];
    
    NSLog(@"imageNameFromTstamp: %@",imageNameFromTstamp);
    if(self.imageString != nil && ![self.imageString isKindOfClass:[NSNull class]]){
        if(![((NSString*)self.imageString) isEqualToString:@""]){
            if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",imageNameFromTstamp]]]){
                // if png image exists:
                //Load Image From Directory
                cellImage = [images loadImage:imageNameFromTstamp ofType:@"png" inDirectory:documentsPath];
            }
            else if([[NSFileManager defaultManager] fileExistsAtPath:[documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",imageNameFromTstamp]]]){
                // if jpg image exists:
                //Load Image From Directory
                cellImage = [images loadImage:imageNameFromTstamp ofType:@"jpg" inDirectory:documentsPath];
            }
            else {
                NSLog(@"imageName: %@", imageName);
                //Get Image From URL
                UIImage * imageFromURL = [images getImageFromURL:[NSString stringWithFormat:@"%@/%@?w=%f",SYSTEM_DOMAIN,imageName,[UIScreen mainScreen].bounds.size.width*2]];
                NSString *imageType = @"";
                if([imageName hasSuffix:@".png"] ||
                   [imageName hasSuffix:@".PNG"]){
                    imageType = @"png";
                }
                else if([imageName hasSuffix:@".jpg"] ||
                        [imageName hasSuffix:@".jpeg"] ||
                        [imageName hasSuffix:@".JPEG"] ||
                        [imageName hasSuffix:@".JPG"]){
                    imageType = @"jpg";
                }
                
                //Save Image to Directory
                [images saveImage:imageFromURL withFileName:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
                
                //Load Image From Directory
                cellImage = [images loadImage:imageNameFromTstamp ofType:imageType inDirectory:documentsPath];
            }
        }
    }
    //[MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    return cellImage;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)loadImage{
    if(!self.preLoadImage){
        self.cellImage = [self createImageForElement];
        if(self.cellImage != nil){
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
            });
        }
    }
}

-(UITableViewCell*)descriptionTableViewCell{
    
    DetailTableViewCell *c = [self.tableView dequeueReusableCellWithIdentifier:@"elementDescriptionCell"];
    
    c.textViewDescription.text = [[attrString string] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return c;
}

@end
