//
//  StructureOverViewController.h
//  pickalbatros
//
//  Created by User on 10.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "OfferDetailViewController.h"
#import "iFeedbackModel.h"

@interface StructureOverViewController : OfferDetailViewController <
    UICollectionViewDataSource,
    UICollectionViewDelegate,
    NSURLConnectionDelegate,
    UIScrollViewDelegate,
    ifbckDelegate
>

@property (nonatomic) BOOL showSidebarButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideBarButton;
@property (nonatomic) SWRevealViewController *swrvc;

@property (weak, nonatomic) IBOutlet UIView *headerImageBorderView;
@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;

@property (weak, nonatomic) IBOutlet UILabel *headerSubheadlineLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerSubSubHeadlineLabel;

@property (weak, nonatomic) IBOutlet UILabel *exploreMoreLabel;

@property (weak, nonatomic) IBOutlet UIView *descriptionView;

@property (nonatomic, strong) NSMutableData *downloadedData;

@property (nonatomic, strong) NSURL *jsonFileUrl;

@property (nonatomic) int placeID;

@property (nonatomic) NSMutableArray *dataArray,*data;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UIImageView *stovcBackgroundImageView;

@property (weak, nonatomic) IBOutlet UIView *iFeedbackButtonView;
@property (weak, nonatomic) IBOutlet UILabel *iFeedbackButtonLabel;
@property (weak, nonatomic) IBOutlet UIButton *iFeedbackButtonButton;
@property (weak, nonatomic) IBOutlet UIImageView *iFeedbackButtonArrow;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *iFeedbackButtonBackground;

- (IBAction)iFeedbackButtonClicked:(UIButton *)sender;

@property (nonatomic) UIImage *imageForBackground;
@property (nonatomic) NSString *sliderImagesString;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *locationBarButton;
@property (weak, nonatomic) IBOutlet UIImageView *locationButtonImage;
- (IBAction)locationButtonClicked:(id)sender;


@property (weak, nonatomic) IBOutlet UIBarButtonItem *settingsBarButton;
@property (weak, nonatomic) IBOutlet UIImageView *settingsButtonImage;
- (IBAction)settingsButtonClicked:(id)sender;

-(UIImage*)getBackgroundImage:(NSString*)sliderImagesString;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *arrowImages;

-(void)setSideBarGesture;
-(void)setSideBarGestureFor:(SWRevealViewController*)revealViewController;

@property (weak, nonatomic) IBOutlet UIPageControl *pageControll;

@property (weak, nonatomic) IBOutlet UILabel *placeHeaderDescription;
@property (weak, nonatomic) IBOutlet UILabel *placeDescription;

-(UIImage*)loadImageForItem:(NSDictionary*)item;

-(UIViewController*)getViewControllerForIndex:(NSInteger)index delegate:(id<ifbckDelegate>)delegate;

@end
