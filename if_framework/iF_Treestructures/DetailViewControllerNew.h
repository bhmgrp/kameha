//
//  DetailViewControllerNew.h
//  pickalbatros
//
//  Created by User on 24.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewControllerNew : UIViewController <UITableViewDataSource, UITableViewDelegate, UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak) NSString *headerText1;
@property (weak) NSString *headerText2;
@property (weak) NSString *descriptionText;
@property (weak) NSString *priceText;

@property (weak) NSString *imageString;
@property (nonatomic) int viewType;

@property (nonatomic) int uid, tstamp;

@property (nonatomic) BOOL ignoreImages,preLoadImage;

@property (weak, nonatomic) IBOutlet UIView *webViewWrapper;
@property (strong, nonatomic) IBOutlet UIWebView *descriptionWebView;

@end
