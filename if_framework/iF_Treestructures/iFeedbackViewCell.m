//
//  iFeedBackViewCell.m
//  pickalbatros
//
//  Created by User on 10.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//
#import "iFeedBackViewCell.h"

@implementation iFeedBackViewCell

-(void)awakeFromNib{
    [super awakeFromNib];
    
    if(self.textAndImageColor == nil)self.textAndImageColor = [UIColor blackColor];
    [self.structureImageViewWrapper.layer setBorderColor:[self.textAndImageColor CGColor]];
    [self.structureImageViewWrapper.layer setCornerRadius:5.0f];
    [self.structureImageViewWrapper.layer setBorderWidth:1.0f];
    
    
    UIImage *templateImage = [self.structureImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.structureImageView.image = templateImage;
}

-(void)updateColors{
    
    UIImage *templateImage = [self.structureImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.structureImageView.image = templateImage;
    [self.structureImageView setTintColor:self.textAndImageColor];
    
    [self.structureNameLabel setTintColor:self.textAndImageColor];
    [self.structureNameLabel setTextColor:self.textAndImageColor];
    [self.structureImageView.layer setBorderColor:[self.textAndImageColor CGColor]];
}

@end
