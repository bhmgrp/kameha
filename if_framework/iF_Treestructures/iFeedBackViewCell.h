//
//  iFeedBackViewCell.h
//  pickalbatros
//
//  Created by User on 10.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface iFeedBackViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *structureNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *structureSubtitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *structureImageView;
@property (weak, nonatomic) IBOutlet UIView *structureImageViewWrapper;

@property (weak) UIColor *textAndImageColor;
-(void) updateColors;

@end
