//
//  StructureImageViewController.h
//  if_framework
//
//  Created by User on 1/22/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StructureImageViewController : UIViewController <UIScrollViewDelegate>


@property (nonatomic) UIImage *image;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet NSString *imageServerPath;

- (IBAction)cancelButtonPressed:(id)sender;

@end
