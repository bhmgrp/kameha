//
//  API.m
//  if_framework
//
//  Created by Julian Böhnke on 16.02.16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "API.h"
#import "Unirest/UNIRest.h"

@implementation API


- (id) request:(NSString *)url :(NSDictionary *)params {
    
    NSDictionary* headers = @{@"accept": @"application/json"};
    
    UNIHTTPJsonResponse *response = [[UNIRest post:^(UNISimpleRequest *request) {
        [request setUrl: url];
        [request setHeaders: headers];
        [request setParameters: params];
    }] asJson];
    
    return response;
}

@end
