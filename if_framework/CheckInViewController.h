//
//  CheckInViewController.h
//  KAMEHA
//
//  Created by User on 20.02.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CheckInDelegate <NSObject>


@end

@interface CheckInViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *arrivalLabel;
@property (weak, nonatomic) IBOutlet UIView *dateFromView;
@property (weak, nonatomic) IBOutlet UILabel *labelFromDay;
@property (weak, nonatomic) IBOutlet UILabel *labelFromMonth;
@property (weak, nonatomic) IBOutlet UILabel *labelFromTime;
@property (strong, nonatomic) NSDate* dateFrom;


@property (weak, nonatomic) IBOutlet UILabel *departureLabel;
@property (weak, nonatomic) IBOutlet UIView *dateToView;
@property (weak, nonatomic) IBOutlet UILabel *labelToDay;
@property (weak, nonatomic) IBOutlet UILabel *labelToMonth;
@property (weak, nonatomic) IBOutlet UILabel *labelToTime;
@property (strong, nonatomic) NSDate* dateTo;

@property (weak, nonatomic) IBOutlet UILabel *wishesLabel;
@property (weak, nonatomic) IBOutlet UITextView *textViewWishes;


@property (weak, nonatomic) IBOutlet UILabel *settingsConfirmedLabel;
@property (weak, nonatomic) IBOutlet UISwitch *settingsSwitch;


@property (weak, nonatomic) IBOutlet UILabel *checkInDescriptionLabel;


@property (strong, nonatomic) IBOutlet UIButton *checkInButton;
- (IBAction)checkInButtonClicked:(UIButton *)sender;


@property (strong, nonatomic) IBOutlet UIButton *registerButton;
@property (strong, nonatomic) UIButton *settingsButton;
@property (weak, nonatomic) IBOutlet UIImageView *settingsButtonImage;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *settingsBarButton;

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *mainViewNotLoggedIn;


@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
- (IBAction)pickerChanged:(UIDatePicker *)sender;
- (IBAction)datePickerDone:(UIButton *)sender;

- (IBAction)settingsButtonClicked:(id)sender;

- (IBAction)switchSettingsConfirmation:(UISwitch *)sender;
@end
