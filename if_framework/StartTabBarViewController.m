//
//  StartTabBarViewController.m
//  if_framework
//
//  Created by User on 3/4/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "StartTabBarViewController.h"

#import "gestgidStartViewController.h"

#import "NAvigationViewController.h"
#import "StructureOverViewController.h"

@interface StartTabBarViewController (){
    int numOfTabs;
}

@property (nonatomic) StructureOverViewController *startViewController;

@property (nonatomic) NSMutableArray *customViewControllers;

@end

@implementation StartTabBarViewController

- (void)viewDidLoad {
    
    [globals sharedInstance];
    
     numOfTabs = NUMBEROFTABSINTABVIEW;
    
    [super viewDidLoad];
    
    [self initStyle];
    
    _customViewControllers = [@[] mutableCopy];

    [self initViewControllers];
    
    // always select the last item (more page)
    if(self.selectedIndex==0)
        [self setSelectedIndex:self.viewControllers.count-1];
}

-(void)initStyle{
    [self.tabBar setTintColor:[UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
    
    [self.tabBar setBarTintColor:[UIColor clearColor]];
    [self.tabBar setTranslucent:YES];
    [self.tabBar setBackgroundImage:[UIImage new]];
    [self.tabBar setShadowImage:[UIImage new]];
    [self.tabBar setBarStyle:UIBarStyleDefault];
    
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIView *barBackground = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    [barBackground setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.tabBar.height)];
    
    [self.tabBar addSubview:barBackground];

    self.moreNavigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    self.moreNavigationController.delegate = self;
    
    [[UIBarButtonItem appearance]setTintColor: [UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// method called, when we switch the tabs
-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    if(item.tag==0){
        //self.tabBar.hidden = 1;
    }
}

-(void)initViewControllers{
    [self setViewControllers:[self configureAndGetViewControllers] animated:YES];
    if(_customViewControllers.count==1){
        self.tabBar.hidden = YES;
    }
}

-(void)updateViewControllers{
    [self updateViewControllersAnimated:NO];
}

-(void)updateViewControllersAnimated:(BOOL)animated{
    [self setViewControllers:_customViewControllers animated:animated];
    if(_customViewControllers.count==1){
        self.tabBar.hidden = YES;
    }

}

-(NSArray*)configureAndGetViewControllers{
    
    NSString *startStoryboardName = @"StructureOverViewInline";
    if(IDIOM==IPAD){
        startStoryboardName = @"StructureOverViewInline-iPad";
    }
    
    _startViewController = [StructureOverViewController loadNowFromStoryboard:startStoryboardName];
    _startViewController.view.tag = 0;

    _customViewControllers = [[NSMutableArray alloc] initWithCapacity:_startViewController.data.count>=4?4:_startViewController.data.count];

    UIImage *tabButton;
    CGSize scaledSize = CGSizeMake(25, 25);
    
    UIImage *tabBarItemImage;
    NSString *tabBarItemTitle;
    
    NSLog(@" customViewControllers: %@", _customViewControllers);

    //// BEGIN tab 0
    if(SINGLEPLACEID==0){
        NSString *gestgidStartStoryboardName = @"gestgidStart";
        if(IDIOM==IPAD){
            gestgidStartStoryboardName = @"gestgidStart-iPad";
        }
        
        UIViewController *svc0 = [gestgidStartViewController loadNowFromStoryboard:gestgidStartStoryboardName]; // should be master view for structure element 1
        
        NavigationViewController *vc0 = [[NavigationViewController alloc] initWithRootViewController:svc0];
        
        // set tab bar item
        vc0.tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemSearch tag:0]; // do this in the view controller itself?
        
        if(vc0!=nil && ![globals sharedInstance].terminalMode)
            [_customViewControllers addObject:vc0];
    }
    //// END tab 0

    
    
    //// BEGIN custom tabs
    for(int i = 0;i<numOfTabs;i++){
        if(_startViewController.data.count>i){
            UIViewController *svc = [_startViewController getViewControllerForIndex:i delegate:self];

            NavigationViewController *vc = [[NavigationViewController alloc] initWithRootViewController:svc];

            if(_customViewControllers.count <= i)
            _customViewControllers[i] = vc;

            svc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"]
                                                                                    style:UIBarButtonItemStylePlain target:svc.revealViewController
                                                                                   action:@selector(revealToggle:)];


            // get / modify image
            tabButton = [_startViewController loadImageForItem:_startViewController.data[i]];

            UIGraphicsBeginImageContextWithOptions(scaledSize, NO, 0.0);
            [tabButton drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
            tabBarItemImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();

            // get title
            tabBarItemTitle = _startViewController.data[i][@"name"];

            // set tab bar item
            vc.tabBarItem = [[UITabBarItem alloc] initWithTitle:tabBarItemTitle image:tabBarItemImage tag:i+1];


            svc.title = tabBarItemTitle;
        }
    }
    
    //// BEGIN Last tab
    if(_startViewController.data.count>numOfTabs){
        NavigationViewController *vc3 = [[NavigationViewController alloc] initWithRootViewController:_startViewController];
        
        _startViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"]
                                                                                                 style:UIBarButtonItemStylePlain target:_startViewController.revealViewController
                                                                                                action:@selector(revealToggle:)];
        
        tabButton = [UIImage imageNamed:@"sb_home.png"];
        
        UIGraphicsBeginImageContextWithOptions(scaledSize, NO, 0.0);
        [tabButton drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
        tabBarItemImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        // get title
        tabBarItemTitle = NSLocalizedString(@"Stay", @"");
        
        // set tab bar item
        vc3.tabBarItem = [[UITabBarItem alloc] initWithTitle:tabBarItemTitle image:tabBarItemImage tag:numOfTabs];
        
        vc3.navigationItem.titleView = [NavigationTitle createNavTitle:tabBarItemTitle SubTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"]];
        
        _startViewController.navigationItem.titleView = [NavigationTitle createNavTitle:tabBarItemTitle SubTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"]];

        _customViewControllers[numOfTabs] = vc3;
    }
    else if(_startViewController.data.count>numOfTabs-1){
        UIViewController *svc = [_startViewController getViewControllerForIndex:numOfTabs-1 delegate:self];
        
        if(svc==nil)svc = [UIViewController load:@"LoadingViewController" fromStoryboard:@"Loading"];
        
        NavigationViewController *vc = [[NavigationViewController alloc] initWithRootViewController:svc];
        
        svc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"]
                                                                                 style:UIBarButtonItemStylePlain target:svc.revealViewController
                                                                                action:@selector(revealToggle:)];
        
        // get / modify image
        tabButton = [_startViewController loadImageForItem:_startViewController.data[numOfTabs-1]];
        
        UIGraphicsBeginImageContextWithOptions(scaledSize, NO, 0.0);
        [tabButton drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
        tabBarItemImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        // get title
        tabBarItemTitle = _startViewController.data[numOfTabs-1][@"name"];
        
        // set tab bar item
        vc.tabBarItem = [[UITabBarItem alloc] initWithTitle:tabBarItemTitle image:tabBarItemImage tag:numOfTabs];
        
        vc.navigationItem.titleView = [NavigationTitle createNavTitle:tabBarItemTitle SubTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"]];
        
        svc.title = tabBarItemTitle;
        
        _customViewControllers[numOfTabs] = vc;
    }
    //// END last tab
    //// END custom tabs
    

    
    
    // return the view controllers
    return _customViewControllers;
}

- (void)setFrame:(CGRect)frame OfChildViewControllersForViewController:(UIViewController*) viewController{
    viewController.view.frame = frame;
    [viewController.view layoutIfNeeded];
    if(viewController.childViewControllers.count>0){
        for(UIViewController *vc in viewController.childViewControllers){
            [self setFrame:frame OfChildViewControllersForViewController:vc];
        }
    }
    if([viewController isKindOfClass:[UINavigationController class]]){
        for(UIViewController *vc in ((UINavigationController *)viewController).viewControllers){
            [self setFrame:frame OfChildViewControllersForViewController:vc];
        }
    }
    if([viewController isKindOfClass:[SWRevealViewController class]]){
        UIViewController *vc = ((SWRevealViewController *)viewController).frontViewController;
        [self setFrame:frame OfChildViewControllersForViewController:vc];
    }
}

- (void)ifbckDidLoadViewController:(UIViewController *)viewController atIndex:(NSInteger)index {

    CGRect frame;

    frame = CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 49);
    UIViewController *vcWrapper = [UIViewController new];
    [vcWrapper.view setFrame: frame];
    UIView *containerView = [[UIView alloc] initWithFrame:frame];
    [vcWrapper.view addSubview:containerView];
    [vcWrapper.view setBackgroundColor:[UIColor whiteColor]];
    [vcWrapper addChildViewController:viewController];
    [self setFrame:frame OfChildViewControllersForViewController:viewController];
    [containerView addSubview:viewController.view];

    [viewController didMoveToParentViewController:vcWrapper];

    UIImage *tabButton;
    CGSize scaledSize = CGSizeMake(25, 25);

    UIImage *tabBarItemImage;
    NSString *tabBarItemTitle;
    tabButton = [_startViewController loadImageForItem:_startViewController.data[index]];

    UIGraphicsBeginImageContextWithOptions(scaledSize, NO, 0.0);
    [tabButton drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
    tabBarItemImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    // get title
    tabBarItemTitle = _startViewController.data[index][@"name"];

    // set tab bar item
    vcWrapper.tabBarItem = [[UITabBarItem alloc] initWithTitle:tabBarItemTitle image:tabBarItemImage tag:index+1];

    _customViewControllers[index] = vcWrapper;

    [self updateViewControllersAnimated:YES];
}

@end