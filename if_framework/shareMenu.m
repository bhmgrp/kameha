//
//  shareMenu.m
//  if_framework
//
//  Created by User on 30.06.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "shareMenu.h"

@implementation shareMenu

+(void)shareDialogueInViewController:(UIViewController*)currentViewController fromSourceView:(UIView*)sourceView{
    NSString *textToShare;
    
    textToShare = NSLocalizedString(@"",@"");
    
    //screenshot of the current webcontent
    //UIGraphicsBeginImageContext(self.view.bounds.size);
    //[self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    //UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    //UIImage *viewImage = [UIImage imageNamed:@""];
//    
//    NSString *urlString = @"";
//    NSString *br = @"";
//    NSString *downloadText = @"";
    
    
    
    NSArray* dataToShare = @[textToShare];
    
    UIActivityViewController* activityViewController = [[UIActivityViewController alloc] initWithActivityItems:dataToShare applicationActivities:nil];
    
    [activityViewController setValue:NSLocalizedString(@"gestgid - Hotel App",@"") forKeyPath:@"subject"];
    activityViewController.popoverPresentationController.sourceView = sourceView;
    [currentViewController presentViewController:activityViewController animated:YES completion:^{}];
    
    
    //NSLog(@"sharemenu");
}
@end
