//
//  LocalStorage.h
//  pickalbatros
//
//  Created by User on 12.05.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface localStorage : NSObject

@property (nonatomic) NSString *fileName;
@property (nonatomic) NSString *filePath;


+(localStorage*)defaultStorage;
+(localStorage*)storageWithFilename:(NSString*)filename;
+(localStorage*)storageWithFilename:(NSString*)filename atPath:(NSString*)path;

-(BOOL)writeDictionary:(NSDictionary*)dict forKey:(id)key;

-(id)getObjectForKey:(id)key;
-(id)objectForKey:(id)key;

-(void)setObject:(id)object forKey:(id<NSCopying>)key;

-(void)removeObjectForKey:(id<NSCopying>)key;

-(void)clearFile;


-(void)logCurrentContent;


@end
