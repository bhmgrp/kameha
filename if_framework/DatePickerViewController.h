//
//  DatePickerViewController.h
//  KAMEHA
//
//  Created by User on 25.02.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckInViewController.h"

@interface DatePickerViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, weak) CheckInViewController *checkInViewControllerDelegate;

@end
